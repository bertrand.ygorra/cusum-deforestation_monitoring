#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Post processing on the CuSum method
Created on Thu Mar 24 12:54:40 2022

@author: bygorra
"""
from osgeo               import gdal,osr,ogr
from tqdm                import tqdm
from scipy               import ndimage
from gistools.layer      import PolygonLayer
import geopandas as gpd
import numpy     as np
import warnings
import fiona
import os

gdal.PushErrorHandler('CPLQuietErrorHandler')
warnings.filterwarnings("ignore")

#%%------------------------------------------------------------------------------ 
# STEP 1 : Useful functions for GIS processing in python
#--------------------------------------------------------------------------------


def save_as_tif(raster_reference, input_array, output_filepath):
    """
    

    Parameters
    ----------
    raster_reference : Gdal image
        Gdal raster of reference, obtained using gdal.Open(file).
    input_array : list or 3d array
        List of arrays you want to save in the file (or 3d array).
    output_filepath : string
        Path of the file to save the arrays in.

    Returns
    -------
    None.

    """
    ds            = raster_reference                                            # Get information from reference raster dataset
    geotransform  = ds.GetGeoTransform()                                        
    wkt           = ds.GetProjection()
    if type(input_array) == type(list(input_array)):                            # Get the numbers of column, rows and arrays in the data
        nb_arrays = len(input_array)
        nb_row, nb_col = input_array[0].shape;
    else:
        nb_dim = len(input_array.shape)
        if nb_dim>2:
            nb_arrays = (input_array.shape)[2]
            nb_row, nb_col = input_array[:,:,0].shape
        else:
            nb_arrays = 1
            nb_row, nb_col = input_array.shape
    
    driver = gdal.GetDriverByName("GTiff");                                     # Create GTiff image
    dst_ds = driver.Create(output_filepath,nb_col,nb_row,nb_arrays,gdal.GDT_Float32)
    if nb_arrays > 1:
        if type(input_array) == type(list(input_array)):
            for curband in range(len(input_array)):
                cur_array = input_array[curband]
                new_array = np.array(cur_array)
                dst_ds.GetRasterBand(curband+1).WriteArray( new_array )         # Writing output raster
                dst_ds.GetRasterBand(curband+1).SetNoDataValue(0)               # Setting nodata value
                dst_ds.SetGeoTransform(geotransform)                            # Setting extension of output raster
                srs = osr.SpatialReference()                                    # Setting spatial reference of output raster
                srs.ImportFromWkt(wkt)
                dst_ds.SetProjection( wkt )
        elif type(input_array) == type(np.array(input_array)):
            for logi in range(input_array.shape[2]):
                cur_array = input_array[:,:,logi]
                new_array = np.array(cur_array)
                
                #writting output raster
                dst_ds.GetRasterBand(logi+1).WriteArray( new_array )
                #setting nodata value
                dst_ds.GetRasterBand(logi+1).SetNoDataValue(0)
                #setting extension of output raster
                # top left x, w-e pixel resolution, rotation, top left y, rotation, n-s pixel resolution
                dst_ds.SetGeoTransform(geotransform)
                # setting spatial reference of output raster
                srs = osr.SpatialReference()
                srs.ImportFromWkt(wkt)
                dst_ds.SetProjection( wkt )        
        #Close output raster dataset
    else:
        new_array = np.array(input_array)
        
        #writting output raster
        dst_ds.GetRasterBand(1).WriteArray( new_array )
        #setting nodata value
        dst_ds.GetRasterBand(1).SetNoDataValue(0)
        #setting extension of output raster
        # top left x, w-e pixel resolution, rotation, top left y, rotation, n-s pixel resolution
        dst_ds.SetGeoTransform(geotransform)
        # setting spatial reference of output raster
        srs = osr.SpatialReference()
        srs.ImportFromWkt(wkt)
        dst_ds.SetProjection( wkt )
    #Close output raster dataset                 
    ds    = None
    dst_ds = None   
def open_tif(filepath,band):
    img = gdal.Open(filepath)
    img_band = img.GetRasterBand(band)
    img_arr = img_band.ReadAsArray()
    img_arr[np.isnan(img_arr)]=0
    img_arr[np.isinf(img_arr)]=0
    img_arr[img_arr==999] = 0
    return img_arr 
def zone_img_creation(ref_filepath,zone_filepath):
    img = gdal.Open(ref_filepath)
    img_arr = open_tif(ref_filepath,1)
    img_arr[:]=1
    save_as_tif(img,img_arr,zone_filepath)
def VV_VH_multi_dates(VV_filepath,VH_filepath,output_filepath):
    """
    Intersection between 2 multidetection output .tif images with multi band
    or NRT simulation over time
    if single NRT computation, please use the VV_VH function

    Parameters
    ----------
    VV_filepath : str
        DESCRIPTION.
    VH_filepath : str
        DESCRIPTION.
    output_filepath : str
        DESCRIPTION.

    Returns
    -------
    None.

    """
    VV_im = gdal.Open(VV_filepath)
    nb_bands = VV_im.RasterCount
    # print('Working on ', VV_filepath,' with ', nb_bands, ' bands')
    X = VV_im.RasterXSize
    Y = VV_im.RasterYSize
    array_3d = np.zeros((Y,X,nb_bands))
    for band in range(nb_bands):
        if band%10 ==0:
            print(band, '\t/\t', nb_bands)
        
        VV = open_tif(VV_filepath,band+1) 
        VH = open_tif(VH_filepath,band+1) 
        
        VV[VV>0] = 1
        VH[VH>0] = 1
        
        VV_VH = VV * VH
        array_3d[:,:,band] = VV_VH
    temp_arr = array_3d[:,:,1:nb_bands-1]
    
    array_3d[:,:,0] = np.sum(temp_arr,axis=2)
    save_as_tif(VV_im,array_3d,output_filepath)
def VV_VH(VV_filepath,VH_filepath,output_filepath):
    """
    Compute VV x VH combination for single band rasters

    Parameters
    ----------
    VV_filepath : str
        Path to the VV .tif file.
    VH_filepath : str
        Path to the VH .tif file.

    Returns
    -------
    None.

    """
    img_ref = gdal.Open(VV_filepath)
    VV = open_tif(VV_filepath,1)
    VH = open_tif(VH_filepath,1)
    
    # VV[VV>0] = 1
    VH[VH>0] = 1
    
    VV_VH = VV * VH
    
    save_as_tif(img_ref,VV_VH,output_filepath)
    img_ref = None
    return output_filepath
def VV_VH_bilan(VV_filepath,VH_filepath,output_filepath):
    """
    Compute VV x VH combination for multi band rasters as a bilan

    Parameters
    ----------
    VV_filepath : str
        Path to the VV .tif file.
    VH_filepath : str
        Path to the VH .tif file.

    Returns
    -------
    None.

    """
    img_ref = gdal.Open(VV_filepath)
    nb_band = img_ref.RasterCount
    temp = img_ref.GetRasterBand(1).ReadAsArray()
    VV_arr = np.zeros((temp.shape[0],temp.shape[1],nb_band))
    VH_arr = np.zeros((temp.shape[0],temp.shape[1],nb_band))
    for band in range(nb_band):
        temp_VV = open_tif(VV_filepath,band+1)
        temp_VH = open_tif(VH_filepath,band+1)
        VV_arr[:,:,band] = temp_VV
        VH_arr[:,:,band] = temp_VH

    VV_sum = np.sum(VV_arr,axis = 2)
    VH_sum = np.sum(VH_arr,axis = 2)
    print('Erreur ici ?')
    VV_VH  = VV_sum * VH_sum
    print('Oui ou non ?')
    bin_temp = np.ones(VV_VH.shape)
    bin_temp[VV_VH<=0] = 0
    for band in range(VH_arr.shape[2]):
        VV = VV_arr[:,:,band]
        temp_arr = np.zeros(VV.shape)
        temp_arr[bin_temp>0]=VV[bin_temp>0]
        
        VV_VH[temp_arr>0] = temp_arr[temp_arr>0]
        
        bin_temp[temp_arr>0] = 0
    save_as_tif(img_ref,VV_VH,output_filepath)
    img_ref = None
    return output_filepath
def reorganize_bands(input_filepath,filelist_date,output_filepath):
    """
    Reorganize the bands of a multidetection .tif output in the temporal order
    of the dates of change

    Parameters
    ----------
    input_filepath : str
        DESCRIPTION.
    filelist_date : list of datetime objects
        Dates of change to find and reorganize into bands
    output_filepath : str
        DESCRIPTION.

    Returns
    -------
    None.

    """
    image = gdal.Open(input_filepath)
    nb_bands = image.RasterCount
    
    array_3d = np.zeros((image.RasterYSize,image.RasterXSize,len(filelist_date)))
    logi_date = 0
    for date in tqdm(filelist_date):
        dateint = int (date.strftime('%Y%m%d'))
        for band in range(nb_bands):
            img_band = image.GetRasterBand(band+1)
            img_arr = img_band.ReadAsArray()
            index = np.where(img_arr == dateint)
            cur_arr = array_3d[:,:,band]
            cur_arr[index] = dateint
            array_3d[:,:,logi_date] = cur_arr
        logi_date+=1
    save_as_tif(image,array_3d,output_filepath)
def nb_change_removal(path,index):
    im = open_tif(path,1)
    im[index]=0
    return im
def remove_pixels_from_mask_raster(filepath,mask_filepath):
    img = gdal.Open(mask_filepath)
    img_cur = open_tif(filepath,1)
    mask = open_tif(mask_filepath,1)
    index = np.where(mask>0)
    img_cur[index]=0
    save_as_tif(img,img_cur,filepath)
    return img_cur
def reduce_bands(input_filepath,output_filepath):
    """
    Function to reduce the 0 arrays in a cusum multidetection output. NOT TO USE
    

    Parameters
    ----------
    input_filepath : TYPE
        DESCRIPTION.
    output_filepath : TYPE
        DESCRIPTION.

    Returns
    -------
    None.

    """
    image = gdal.Open(input_filepath)
    nb_bands = image.RasterCount
    
    band_list = list()
    for band in range(nb_bands):
        img_band = image.GetRasterBand(band+1)
        img_arr = img_band.ReadAsArray()
        # print(np.unique(img_arr))
        if len(np.where(img_arr>1000)[0])>5:
            band_list.append(img_arr)

    save_as_tif(image,band_list,output_filepath)
def cleanup(filepath,band):
    """
    Remove the file after getting the array.

    Parameters
    ----------
    filepath : TYPE
        DESCRIPTION.
    band : TYPE
        DESCRIPTION.

    Returns
    -------
    img_arr : TYPE
        DESCRIPTION.

    """
    img = gdal.Open(filepath)
    img_band = img.GetRasterBand(band)
    img_arr = img_band.ReadAsArray()
    os.remove(filepath)
    return img_arr
def progress_cb(complete, message, cb_data):
    '''Emit progress report in numbers for 10% intervals and dots for 3%'''
    if int(complete*100) % 10 == 0:
        if int(complete*100) % 100 == 0:
            print(f'{complete*100:.0f}', end='\n', flush=True)
        else: 
            print(f'{complete*100:.0f}', end='', flush=True)
    elif int(complete*100) % 3 == 0:
        print(f'{cb_data}', end='', flush=True)
    # print('\n')
def combine_rasters(path_raster_to_keep_values,path_raster_to_combine):
    """
    

    Parameters
    ----------
    path_raster_to_keep_values : str
        Path to the raster to remove values of.
    path_raster_to_combine : str
        Path to the raster to get the index of values to remove.

    Returns
    -------
    raster_to_keep_values : TYPE
        DESCRIPTION.

    """
    raster_to_keep_values = open_tif(path_raster_to_keep_values,1)
    raster_to_combine = open_tif(path_raster_to_combine,1)
    
    index_to_remove = np.where(raster_to_combine <=0)
    raster_to_keep_values[index_to_remove]=0
    return raster_to_keep_values
def progress_callback(complete, message, data):
    percent = int(complete * 100)  # round to integer percent
    data.update(percent)  # update the progressbar
    return 1
def remove_shapefile(shapefile):
    if os.path.exists(shapefile):
        os.remove(shapefile)
        os.remove(shapefile.replace('.shp','.shx') )
    else:
        print('Shapefile not found')
    if os.path.exists(shapefile.replace('.shp','.prj')):
        os.remove(shapefile.replace('.shp','.prj') )
    os.remove(shapefile.replace('.shp','.dbf') )
    if os.path.exists(shapefile.replace('.shp','.cpg')):
        os.remove(shapefile.replace('.shp','.cpg') )
def repair_shapefile(input_filepath):
    """
    Code to solve the self-intersection polygons of the polygonize_raster output

    Parameters
    ----------
    input_filepath : str
        Path to the .shp to repair the self intersections of.

    Returns
    -------
    None.

    """
    # print('Repairing ', os.path.basename(input_filepath))
    gk500 = ogr.Open(input_filepath, 1) # <====== 1 = update mode
    gk_lyr = gk500.GetLayer()
    # print(len(gk_lyr), ' features to check')
    for feature in gk_lyr:
        # logi += 1
        # if len(gk_lyr)!=0:
            # if logi%int(len(gk_lyr)/10) == 0:
                # print( str(logi/len(gk_lyr)*100).split('.')[0], ' % PROGRESS')
        geom = feature.GetGeometryRef()
        if not geom.IsValid():
            feature.SetGeometry(geom.Buffer(0)) # <====== SetGeometry
            gk_lyr.SetFeature(feature) # <====== SetFeature
            assert feature.GetGeometryRef().IsValid() # Doesn't fail

    gk_lyr.ResetReading()
    assert all(feature.GetGeometryRef().IsValid() for feature in gk_lyr) 
def split_translate(input_filepath,chunk_number,x,y,x1,y1):
    """
    Code that splits the .tif file in input_filepath into a chunk size x,y,x1,y1

    Parameters
    ----------
    input_filepath : str
        Path to the .tif file to split.
    chunk_number : int
        Number of the chunk. Only added for reference in a path.
    x : int
        Start at x column.
    y : int
        start at y row.
    x1 : int
         End at x + x1 columns.
    y1 : int
        End at y + y1 rows.

    Returns
    -------
    output_filepath : str
        CHunk filepath.

    """
 
    output_filepath = os.path.join(os.path.dirname(input_filepath),os.path.basename(input_filepath).replace('.tif','_chunk_'+str(chunk_number)+'.tif'))
    # print('...................SPLITTING DATA.................')
    com_string = "gdal_translate -srcwin " + str(x)+ " " + str(y)+ " " + str(x1) + " " + str(y1) + " "  + input_filepath + " " + output_filepath 
    os.system(com_string)
    return output_filepath   
def slice_array(input_filepath,slicing_number):
    """
    Function to slice an array into slicing_number number of chunks

    Parameters
    ----------
    input_filepath : str
        Path to the .tif file containing the array.
    slicing_number : int
        Number of chunks to slice. Must be 2^x.

    Returns
    -------
    None.

    """
    filepath_output_list = list()
    
    img = open_tif(input_filepath,1)
    nb_Y,nb_X = img.shape
    tilesize_X = int(nb_X/slicing_number)
    tilesize_Y = int(nb_Y/slicing_number)
    chunk_number = 1
    
    print('\n\n Slicing the array into chunks\n\n')
    for slice_x in range(slicing_number):
        x  = (slice_x  )*int(nb_X/slicing_number)
        
        if slice_x == slicing_number -1:
            tilesize_X = int(nb_X-x)
        for slice_y in range(slicing_number):
            y  = (slice_y  )*int(nb_Y/slicing_number)
            if slice_y == slicing_number -1:
                tilesize_Y = int(nb_Y-y)
            # print('\nx value :\t',x,'\ttile size X value :\t', tilesize_X,'\ny value :\t',y,'\tTile size Y value :\t',tilesize_Y,'\n\n')
            chunk_filepath= split_translate(input_filepath , chunk_number, x, y, tilesize_X, tilesize_Y)
            filepath_output_list .append(chunk_filepath )
            chunk_number+=1
    return filepath_output_list
    


""" 
#%%------------------------------------------------------------------------------ 
# STEP 2 : Spatial recombination of the temporal results : cross-Tc
#--------------------------------------------------------------------------------
"""

# Recommended steps :
# (1) Binarization of both high and low tc maps (100 and 75), in all polarizations

def binarization(input_filepath,output_filepath):
    """
    Binarization of the input file.
    
    Parameters
    ----------
    input_filepath : str
        filepath to ;tif file to binarize.
    output_filepath : str
        DESCRIPTION.

    Returns
    -------
    None.

    """
    img = gdal.Open(input_filepath)
    array = open_tif(input_filepath,1)
    
    array[np.where(array==0)] = 0
    array[np.where(array>0)] = 1
    array[np.where(np.isnan(array)==True)] = 0
    array[np.where(np.isinf(array)==True)] = 0
    save_as_tif(img,array,output_filepath)
def dilate_erode(filepath,band):
    """
    Dilation and erosion of the matrix composing the bands of a multidetection .tif output
    to fill small gaps in the data.

    Parameters
    ----------
    filepath : str
        Path to the bilan .tif file.
    band : int
        Number of the band to dilate.

    Returns
    -------
    None.

    """
    # print('\n---------------------------------------------------------\nDilating, eroding ' + os.path.basename(filepath))
    # print('\n DIRECTORY PATH : ', os.path.dirname(filepath))
    img_file = gdal.Open(filepath)
    img_arr = open_tif(filepath,1)
    output_ero = os.path.join(os.path.dirname(filepath),'dil_'+os.path.basename(filepath))
    img_arr[img_arr>=1] = 1
    img_arr[img_arr!=1] = 0
    # img_dil  = ndimage.binary_dilation(img_arr ).astype(img_arr.dtype)
    # img      = ndimage.binary_erosion (img_dil ).astype(img_dil.dtype)
    # img_ero  = ndimage.binary_erosion (img     ).astype(img    .dtype)
    # img      = ndimage.binary_dilation(img_ero ).astype(img_ero.dtype)
    save_as_tif(img_file,img_arr,output_ero)
    img_file = None
    return output_ero    

# (2) Remove non-forest seen by S1-Mask :
    
def remove_anterior_pixel(file_historic,current_file,output_file):
    """
    Remove the historic changes (file historic) from the current file (Single change output map)

    Parameters
    ----------
    file_historic : str
        path to the historic file.
    current_file : str
        Path to the file to remove the historic in.

    Returns
    -------
    None

    """
    img_ref = gdal.Open(current_file)
    anterior_arr = open_tif(file_historic,1)
    nan_index = np.where(anterior_arr>0)
    img_ref = gdal.Open(file_historic)
    arr = open_tif(current_file,1)
    arr[nan_index]=0
    save_as_tif(img_ref,arr,output_file)

# (3) RECUSUM ONLY : Remove pixels showing changes > threshold (VH)

def band_sum(input_filepath,output_filepath):
    """
    Compute the number of changes of a multidetection .tif output.

    Parameters
    ----------
    input_filepath : str
        DESCRIPTION.
    output_filepath : str
        DESCRIPTION.

    Returns
    -------
    None.

    """
    image = gdal.Open(input_filepath)
    nb_bands = image.RasterCount
    X = image.RasterXSize
    Y = image.RasterYSize
    array_3d = np.zeros((Y,X,nb_bands))
    
    for band in range(nb_bands):
        img_band = image.GetRasterBand(band+1)
        img_arr = img_band.ReadAsArray()
        img_arr[img_arr>1000] = 1
        array_3d[:,:,band] = img_arr
    
    nb_change_array = np.sum(array_3d, axis = 2)
        
    save_as_tif(image,nb_change_array,output_filepath)
def gradient_analysis(abcisse,value_series):
    grad_output = list()
    for logi in range(1,len(abcisse)):
        cur_grad =(value_series[logi]-value_series[logi-1])/(abcisse[logi]-abcisse[logi-1])
        grad_output.append(cur_grad)
    maximum_gradient_index = np.where(np.array(grad_output)==np.max(np.array(grad_output)))
    maximum_gradient = abcisse[maximum_gradient_index]
    return maximum_gradient
def automatic_threshold(nb_change_arr_VH,nb_change_interval):
    values = np.arange(nb_change_interval[0],nb_change_interval[1]+1)
    val_count = list()
    for val in values:
        nb_pix = len(np.where(nb_change_arr_VH==val)[0])
        val_count.append(nb_pix)
    estimated_threshold = gradient_analysis(values,val_count)
    return estimated_threshold
def automatic_remove_nb_change(nb_change_interval_high,nb_change_interval_low,filepath,filepath_95_VH,filepath_100_VH):
    """
    Post processing step consisting in removing pixels showing a higher number of changes 
    in the high Tc (100) VH file OR in the low Tc (95) VH file. See Ygorra et al., 2022
    for precisions on how to set the 2 thresholds.

    Parameters
    ----------
    nb_change_interval_high : int
        Number of changes interval to base the automatic detection of best threshold on.
        It is used on the VH 100 file input.
    nb_change_interval_low : int
        Number of changes interval to base the automatic detection of best threshold on.
        It is used on the VH 95 file input.
    filepath : str
        Filepath to the image to remove the number of changes on.
    filepath_95_VH : str
        Path to the VH Tc = 95 file.
    filepath_100_VH : str
        Path to the VH Tc = 100 file.

    Returns
    -------
    array : numpy array
        Array of the input file with the pixels showing values higher than the threshold
        removed.
    high_nb_change_threshold : int
        Output threshold based on the VH 100 file.
    low_nb_change_threshold : int
        Output threshold based on the VH 95 file.

    """
    array_100 = open_tif(filepath_100_VH,1)
    array_95 = open_tif(filepath_95_VH,1)
    
    high_nb_change_threshold = automatic_threshold(array_100,nb_change_interval_high)
    low_nb_change_threshold = automatic_threshold(array_95,nb_change_interval_low)
    index_high = np.where(array_100>=high_nb_change_threshold)
    index_low  = np.where(array_95 >=low_nb_change_threshold)
    array = open_tif(filepath,1)
    array[index_high]=0
    array[index_low ]=0
    return array,high_nb_change_threshold,low_nb_change_threshold

def remove_nb_change(threshold_low,threshold_high,filepath,filepath_95_VH,filepath_100_VH):
    """
    Post processing step consisting in removing pixels showing a higher number of changes 
    in the high Tc (100) VH file OR in the low Tc (95) VH file. See Ygorra et al., 2022
    for precisions on how to set the 2 thresholds.

    Parameters
    ----------
    threshold_low : int
        Maximum number of change over which a pixel is considered non-forest.
        It is used on the VH 95 file input.
    threshold_high : int
        Maximum number of change over which a pixel is considered non-forest.
        It is used on the VH 95 file input.
    filepath : str
        Filepath to the image to remove the number of changes on.
    filepath_95_VH : str
        Path to the VH Tc = 95 file.
    filepath_100_VH : str
        Path to the VH Tc = 100 file.

    Returns
    -------
    array : numpy array
        Array of the input file with the pixels showing values higher than the threshold
        removed.
    high_nb_change_threshold : int
        Output threshold based on the VH 100 file.
    low_nb_change_threshold : int
        Output threshold based on the VH 95 file.

    """
    array_100 = open_tif(filepath_100_VH,1)
    array_95 = open_tif(filepath_95_VH,1)
    array = open_tif(filepath,1)

    index_high = np.where(array_100>=threshold_high)
    index_low  = np.where(array_95 >=threshold_low)

    array[index_high]=0
    array[index_low ]=0
    img = gdal.Open(filepath_100_VH)
    save_as_tif(img,array,filepath)

# (2) Conversion of both high/low tc binar maps to shapefiles

def polygonize_raster(input_filepath,output_filepath):
    """
    Function to polygonize a .tif file in a .shp output.

    Parameters
    ----------
    input_filepath : str
        .tif file to convert.
    output_filepath : str
        Do i really have to describe it ?

    Returns
    -------
    None.

    """
    src_ds = gdal.Open(input_filepath)
    # print(output_filepath)
    srs = osr.SpatialReference()
    srs.ImportFromWkt(src_ds.GetProjection())
    srcband = src_ds.GetRasterBand(1)


    #
    #  create output datasource
    #
    dst_layername = "POLYGONIZED_STUFF"
    drv = ogr.GetDriverByName("ESRI Shapefile")
    dst_ds = drv.CreateDataSource( output_filepath)
    dst_layer = dst_ds.CreateLayer(dst_layername, srs = srs )
    DN_field = ogr.FieldDefn('DN', ogr.OFTReal)
    dst_layer.CreateField(DN_field)
    gdal.Polygonize( srcband, srcband, dst_layer, 0, [], callback=None )
    dst_ds = None

# (3) Spatial recombination : cross-tc -- METHOD 6 is the one working fastest as for loop.

def area_filter(geodataframe,area_threshold):
    """
    Code to filter a geodataframe according to the polygon area in square meters.

    Parameters
    ----------
    geodataframe : geopandas.geodataframe
        Geopandas geodataframe to remove of the polygons showing a lower area
        than the threshold.
    area_threshold : int
        Area threshold in square meters.

    Returns
    -------
    geodataframe
        geopandas geodataframe composed by polygons showing a higher area than the threshold.

    """
    if geodataframe.crs == None:
        print('Wrong CRS switching to 4326')
        with fiona.Env(OSR_WKT_FORMAT="WKT2_2018"):
            geodataframe.crs = 'EPSG:4326'   
    else:
        with fiona.Env(OSR_WKT_FORMAT="WKT2_2018"):
            temp = geodataframe.to_crs('EPSG:3857')
            mask = temp.area > area_threshold
            # print(len(mask))
            masked_data = geodataframe.loc[mask]
            output = masked_data.to_crs('EPSG:4326')
    return output

def method_5(high_tc,low_tc,area_threshold):
    """
    Cross-Tc computation : Working on the 'intersection' rule. If a low Tc polygon shows
    AT LEAST one intersection with a high tc polygon, it is not removed.

    Parameters
    ----------
    high_tc : geopandas file
        High Tc (100) geopandas opened file.
    low_tc : geopandas file
        Low Tc (75) geopandas opened file.
    area_threshold : int
        Threshold based on the area (square meters) to remove the polygons showing
        a lower area.

    Returns
    -------
    output : geopandas geodataframe
        DESCRIPTION.

    """        
    
    crs_tc = high_tc.to_crs('EPSG:3857')
    crs_low = low_tc.to_crs('EPSG:3857')
    filtered_high_tc = area_filter(crs_tc,area_threshold )
    filtered_low_tc  = area_filter(crs_low,area_threshold)
    filtered_high_crs = filtered_high_tc.to_crs('EPSG:4326')
    filtered_low_crs  = filtered_low_tc .to_crs('EPSG:4326')
    filtered_high_crs.to_file   (driver = 'ESRI Shapefile', filename = os.path.abspath(os.path.join(os.getcwd(),'temp_high.shp')))
    filtered_low_crs .to_file   (driver = 'ESRI Shapefile', filename = os.path.abspath(os.path.join(os.getcwd(),'temp_low.shp')))
    
    # print('area filtered shapefile : ',filtered_high_crs,'\n length of low tc : ',len(filtered_low_crs.geometry))
    # geohigh = PolygonLayer.from_collection(os.path.abspath(os.path.join(os.getcwd(),'temp_high.shp')))
    geohigh = PolygonLayer(os.path.abspath(os.path.join(os.getcwd(),'temp_high.shp')))
    geolow = PolygonLayer(os.path.abspath(os.path.join(os.getcwd(),'temp_low.shp')))
    bool_output = geolow.intersects(geohigh)
    output = geolow[bool_output]
    # print('TEST ON GEOLAYER METHOD : ', output)
    remove_shapefile(os.path.abspath(os.path.join(os.getcwd(),'temp_high.shp')))
    remove_shapefile(os.path.abspath(os.path.join(os.getcwd(),'temp_low.shp')))
    return output        
def method_6(high_tc,low_tc,area_threshold):
    """
    Cross-Tc computation : Working on the 'intersection' rule. If a low Tc polygon shows
    AT LEAST one intersection with a high tc polygon, it is not removed.

    Parameters
    ----------
    high_tc : geopandas file
        High Tc (100) geopandas opened file.
    low_tc : geopandas file
        Low Tc (75) geopandas opened file.
    area_threshold : int
        Threshold based on the area (square meters) to remove the polygons showing
        a lower area.

    Returns
    -------
    output : geopandas geodataframe
        DESCRIPTION.

    """        

    filtered_high_crs = area_filter(high_tc,area_threshold)
    filtered_low_crs  = area_filter(low_tc ,area_threshold)

    if not filtered_high_crs.empty:
        with fiona.Env(OSR_WKT_FORMAT="WKT2_2018"):


            # print('area filtered shapefile : ',filtered_high_crs,'\n length of low tc : ',len(filtered_low_crs.geometry))
            # geohigh = PolygonLayer.from_collection(os.path.abspath(os.path.join(os.getcwd(),'temp_high.shp')))
            output_high = os.path.abspath(os.path.join(os.getcwd(),'temp_high.shp'))
            duplicate_count = 1
            exist = False
            if os.path.exists(output_high):
                a = output_high.replace('.shp','_' + str(duplicate_count) + '.shp')
                if os.path.exists(a):
                    exist = True
                else:
                    b = a
                while exist == True:
                    duplicate_count += 1

                    b = os.path.abspath(os.path.join(os.getcwd(),'temp_high' + str(duplicate_count) + '.shp'))
                    if os.path.exists(b):
                        exist = True
                    else:
                        exist = False
                    # print('While loop still working : ', duplicate_count, ' ',b)
            else:
                b = output_high
                
            output_low = b.replace('high','low')
            # print('High tc file empty :\t',filtered_high_crs.empty)
            # print('Low tc file empty :\t',filtered_low_crs  .empty)
            if filtered_high_crs.crs == None:
                filtered_high_crs.crs = 'EPSG:4326'
            if filtered_low_crs.crs == None:
                filtered_low_crs.crs = 'EPSG:4326'
            filtered_high_crs.to_file   (driver = 'ESRI Shapefile', filename = b)
            filtered_low_crs .to_file   (driver = 'ESRI Shapefile', filename = output_low)             
            
        geohigh = PolygonLayer(b)
        geolow = PolygonLayer(output_low)
        bool_output = geolow.intersects(geohigh)
        
        if True in bool_output:
            print('True in bool output, can continue', np.unique(bool_output))
            output = geolow[bool_output]            

        else:
            print('False only in bool output, cannot continue', np.unique(bool_output))
            output = False
            temp = 0
        # print('TEST ON GEOLAYER METHOD : ', output)
        remove_shapefile(b)
        remove_shapefile(output_low)
        temp =4
    else:
        print('Output is empty and THAT is the bug')
        output = False
        temp = 0
    return output,temp  
def cross_tc(high_tc_file,low_tc_file,thresh,thresh_high,area_threshold):
    """
    Cross-Tc spatial recombination.

    Parameters
    ----------
    high_tc_file : str
        Path toe the high Tc VECTOR file (.shp).
    low_tc_file : str
        Path to the low Tc VECTOR file (.shp).
    thresh : int
        Lower Tc (often 75).
    thresh_high : int
        higher Tc (often 100).
    area_threshold : int
        Minimum Mapping Unit (square meters). Removes polygons < to the threshold

    Returns
    -------
    None.

    """
    high_tc_pd = gpd.read_file(high_tc_file)
    low_tc_pd  = gpd.read_file(low_tc_file)
    output_cross,value_indicator = method_6(high_tc_pd,low_tc_pd,area_threshold)  
    if value_indicator !=0:
        output_filepath = low_tc_file.replace(str(thresh),str(thresh_high) + '_' + str(thresh))
        output_cross.to_file   (driver = 'ESRI Shapefile', file_path = output_filepath, crs='EPSG:4326')
        repair_shapefile(output_filepath)
    return value_indicator

# (4) Conversion from cross-tc shape to cross-tc in tif

def gdal_mask(kml_path, input_filepath,output_filepath):
    """
    
    Function to set to '0' all pixels not belonging to the polygons of the vector file..
    Parameters
    ----------
    kml_path : str
        path to your kml or .shp vector file.
    input_filepath : str
        path to your .tif image to mask pixels of.

    Returns
    -------
    str
        path to the output image.

    """
    options = {'cropToCutline':False,'dstNodata':0,\
    'cutlineDSName':kml_path,'callback': progress_cb,\
    'callback_data': '.'}
    
    gdal.Warp(output_filepath, input_filepath,options =gdal.WarpOptions(**options));    
    
def conversion(filelist_low,crosslist,input_folderpath,TEMP_folder,thresh):
    """
    Conversion of a .shp file to a .tif file using gdal_mask to remove pixels of 
    the .tif file outside of the polygons of the .shp file.

    Parameters
    ----------
    filelist_low : list
        List of the names of the .tif files.
    crosslist : list
        List of the cross-tc shapefiles. VECTOR FILES.
    input_folderpath : str
        path to the shapefile folder
    TEMP_folder : TYPE
        DESCRIPTION.
    thresh : int
        Tc threshold.

    Returns
    -------
    None.

    """
    for tif_low in tqdm(filelist_low):
        arr_list = list()
        img_ref = gdal.Open(os.path.join(input_folderpath,tif_low))
        nb_bands = img_ref.RasterCount
        for cross_shp,band in zip(crosslist,range(nb_bands)):
            file_temp    = os.path.join(TEMP_folder,os.path.basename(cross_shp).replace('.shp','.tif'))
            gdal_mask(cross_shp,os.path.join(input_folderpath,tif_low   ),file_temp   )
            print('\n')
            arr_list.append(cleanup(file_temp,band+1))
        save_as_tif(img_ref,arr_list,os.path.join(input_folderpath,tif_low.replace(str(thresh),'100_'+ str(thresh))))
def shp_to_tif(input_filepath_shp,tif_filepath):
    """
    Conversion of the vector file .shp to raster file .tif

    Parameters
    ----------
    input_filepath_shp : str
        Path to the cross tc vector file.
    tif_filepath : str
        Path to the 'area' tif file.

    Returns
    -------
    output_filepath : str
        DESCRIPTION.

    """
    output_filepath = input_filepath_shp.replace('.shp','.tif')
        
    gdal_mask(input_filepath_shp,tif_filepath,output_filepath)
    print("\n")
    if os.path.exists(input_filepath_shp):
        remove_shapefile(input_filepath_shp)

    return output_filepath
def choose_date_change_polarisation(cross_tc_filepath,single_pol_low_tc_input_filepath):
    """
    

    Parameters
    ----------
    cross_tc_filepath : str
        PAth to your cross tc output file.
    single_pol_input_filepath : str
        PAth to your Cusum_main.py low tc output file, either VV (recommended for water) 
        or VH (recommended for forests)

    Returns
    -------
    None.

    """
    arr_cross = open_tif(cross_tc_filepath,1)
    arr_date = open_tif(single_pol_low_tc_input_filepath,1)
    index = np.where(arr_cross==1)
    arr_cross[index]=arr_date[index]
    
    img = gdal.Open(cross_tc_filepath)
    save_as_tif(img, arr_cross, cross_tc_filepath)

