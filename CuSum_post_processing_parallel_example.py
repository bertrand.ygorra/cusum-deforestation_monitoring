#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on  24/012023
Example on how to set and use the postprocessing proposed in ygorra et al 2021 : 
    Monitoring tropical forest cover change from Sentinel-1 : a cusum-based approach
    with the NF mask proposed in ygorra et al 2024 ISPRS  : ReCuSum 
@author: bygorra
"""

import os 
import warnings
import multiprocessing as mp
from osgeo                 import gdal
from CuSum_post_processing import zone_img_creation,VV_VH, dilate_erode,remove_anterior_pixel,binarization,polygonize_raster,repair_shapefile, \
    remove_shapefile, slice_array, cross_tc,shp_to_tif, band_sum, remove_nb_change,choose_date_change_polarisation

    
def parallel_cross_tc(VV_VH_low_pol,VV_VH_high_pol):                            # Definition to implement as a parallel iteration over an index, see pool under.
    cross_tc(VV_VH_high_pol,VV_VH_low_pol,threshold_low,threshold_high,area_threshold)                 
    return 'FINISHED'         
    
    
    

    
"""
#%%------------------------------------------------------------------------------
#    STEP 1 : VV x VH combination, dilation /erosion, remove anterior pixels
#--------------------------------------------------------------------------------
# Recommended steps :
# (1)  Dilatation, erosion
# (2) VV x VH combination
# (3) Historic changes removal   
"""  
if __name__ == '__main__' :

    gdal.PushErrorHandler('CPLQuietErrorHandler')
    warnings.filterwarnings("ignore")

    os_path = os.getcwd();
    if 'Travail_zone_' in os_path:
        os.chdir("..")
        os.chdir("..")
    VV_VH_computation     = True                                                                         # Computation of VV intersect VH change rasters
    dil_ero               = True                                                                         # Dilatation and erosion, then erosion + dilatation to join roads
    remove_historic_pixel = False                                                                         # To remove pixels showing a change in a period before the monitoring (NF mask can fit here)
    ReCuSum               = False
    remove_high_nb_change = False
    nb_change_threshold_high = 8
    nb_change_threshold_low  = 18
    
    VV_Cusum_file_low  = os.path.abspath(os.path.join('your folder path','your VV filename low.tif'))   # Absolute path to your file containing the low tc cusum  VV result from cusum.py
    VV_Cusum_file_high = os.path.abspath(os.path.join('your folder path','your VV filename high.tif'))   # Absolute path to your file containing the low tc cusum  VV result from cusum.py
    
    VH_Cusum_file_low  = os.path.abspath(os.path.join('your folder path','your VH filename low.tif'))   # Absolute path to your file containing the low tc cusum  VV result from cusum.py
    VH_Cusum_file_high = os.path.abspath(os.path.join('your folder path','your VH filename high.tif'))   # Absolute path to your file containing the low tc cusum  VV result from cusum.py
    
    Historic_filepath = os.path.abspath(os.path.join('your folder path','your historic filename.tif')) # Absolute path to your file containing the NF mask (forest coded as >0) / CuSum cross-Tc on anterior period
    
    

    if remove_high_nb_change or ReCuSum:
        VH_nb_change_filepath_95  = os.path.abspath(os.path.join('your folder path','your map of number of changes of VH filename 95 .tif'))   # Absolute path to your file containing the low tc cusum  VH 95 result from ReCuSum post processing
        VH_nb_change_filepath_100 = os.path.abspath(os.path.join('your folder path','your map of number of changes of VH filename 100 .tif'))  # Absolute path to your file containing the low tc cusum  VH 100 result from ReCuSum post processing
    
    if ReCuSum:
        VH_filepath_95  = os.path.abspath(os.path.join('your folder path','your VH filename 95 .tif'))   # Absolute path to your file containing the low tc cusum  VH 95 result from cusum.py as multi-detection 
        VH_filepath_100 = os.path.abspath(os.path.join('your folder path','your VH filename 100 .tif'))   # Absolute path to your file containing the low tc cusum  VH 100 result from cusum.py as multi-detection

    
    if VV_VH_computation:
    # Intersection of VV result map with VH result map : Greatly decrease false positive, slightly increase the false negative.
        print('Intersecting VV with VH....................')
        VV_VH_low  = VV_VH(VV_Cusum_file_low ,VH_Cusum_file_low ,VH_Cusum_file_low .replace('VH','cross'))
        VV_VH_high = VV_VH(VV_Cusum_file_high,VH_Cusum_file_high,VH_Cusum_file_high.replace('VH','cross'))

    if dil_ero:
    # Closing : dilatation dilatation erosion erosion to close gaps / connects roads
        print('Dilating, eroding....................')
        dil_VV_VH_file_low  = dilate_erode(VH_Cusum_file_low .replace('VH','cross'),1)                         
        dil_VV_VH_file_high = dilate_erode(VH_Cusum_file_high.replace('VH','cross'),1)
        
    if remove_historic_pixel:
    # Historic pixels are pixels determined as change before the monitoring period. = Non-Forest mask
        print('Removing anterior pixels....................')
        remove_anterior_pixel(Historic_filepath,dil_VV_VH_file_low ,dil_VV_VH_file_low .replace('.tif','_rm.tif'))
        remove_anterior_pixel(Historic_filepath,dil_VV_VH_file_high,dil_VV_VH_file_high.replace('.tif','_rm.tif'))
            
    if ReCuSum: 
        band_sum(VH_filepath_95 ,VH_nb_change_filepath_95 )
        band_sum(VH_filepath_100,VH_nb_change_filepath_100)
        
    if remove_high_nb_change:
        remove_nb_change(nb_change_threshold_low,nb_change_threshold_high,VV_VH_low ,VH_nb_change_filepath_95,VH_nb_change_filepath_100)
        remove_nb_change(nb_change_threshold_low,nb_change_threshold_high,VV_VH_high,VH_nb_change_filepath_95,VH_nb_change_filepath_100)
        
        
    """
    #%%------------------------------------------------------------------------------ 
    # STEP 2 : Spatial recombination of the temporal results : cross-Tc
    #--------------------------------------------------------------------------------
    
    # Recommended steps :
    # (1) Binarization of both high and low tc maps (100 and 75), in all polarizations
    # (2) Conversion of both high/low tc binar maps to shapefiles
    # (3) Spatial recombination : cross-tc
    # (4) Conversion from cross-tc shape to cross-tc in tif
    """
    # Keep True to all parameters
    binar                   = True                                      
    polygonize              = True
    repair                  = True
    cross_tc_bool           = True
    nb_processeur_para      = 2                                                 # Number of processors to allocate to the parallel processing
    nb_slice                = 2                                                 # Number of slices of your image (subparts)
    total_slices = int(nb_slice * nb_slice)                                     # Total number of chunks obtained by slicing the image vertically and horizontally
    area_threshold          = 300                                               # Minimum mapping unit, in square meters (300 m2)
    threshold_low = 75                                                          # Lower value of the Critical Threshold (Tc). Usually 75
    threshold_high = 100                                                        # Higher value of the Critical Threshold (Tc). Usually 100


    VV_VH_Cusum_file_low  = os.path.abspath(os.path.join('your folder path','filename of intersection file low.tif')) 
    VV_VH_Cusum_file_high = os.path.abspath(os.path.join('your folder path','filename of intersection file high.tif')) 
    whole_zone_path = os.path.join(os.path.dirname(VV_VH_Cusum_file_low),'whole_zone.tif') # whole_zone.tif is a file composed of 1s with the same size as the above files.
    
    
    if not os.path.exists(whole_zone_path):                                     # If this file does not exist, we create it
        print('-------------------\nCreating the zone.tif\n-------------------')
        zone_img_creation(VV_VH_Cusum_file_high,whole_zone_path)

    
    if binar:
        VV_VH_bin_low  = VV_VH_Cusum_file_low .replace('.tif','_bin.tif')        # Name of the binary .tif file created
        VV_VH_bin_high = VV_VH_Cusum_file_high.replace('.tif','_bin.tif')
        
        binarization(VV_VH_Cusum_file_low ,VV_VH_bin_low )                      # Binarization of the low file
        binarization(VV_VH_Cusum_file_high,VV_VH_bin_high)
        
        
    if polygonize:
        output_filepath_list = list()
        low_list  = slice_array(VV_VH_bin_low ,nb_slice)                    # Slices the array and lists the slices .tif files
        high_list = slice_array(VV_VH_bin_high,nb_slice)            
        print('-------------------\nSTARTING POLYGONIZATION........\n-------------------')
        for VV_VH_low, VV_VH_high in zip(low_list,high_list):
            VV_VH_low_pol = VV_VH_low .replace('.tif','.shp')
            VV_VH_high_pol= VV_VH_high.replace('.tif','.shp')
        
            polygonize_raster(VV_VH_low ,VV_VH_low_pol )                    # Polygonize the binary raster. 0s are NaN
            polygonize_raster(VV_VH_high,VV_VH_high_pol)
    
            if repair:
                print('-------------------\nSTARTING SHP FIXATION\n-------------------')
            
                repair_shapefile(VV_VH_low_pol )                                # Eventually repairs the shapefile. 
                repair_shapefile(VV_VH_high_pol)
        
    if cross_tc_bool:
        print('---------------------------------\nSTARTING CROSS-TC COMPUTATION\n---------------------------------')    
        
        low_list = [os.path.join(os.path.dirname(VV_VH_Cusum_file_low),f) for f in os.listdir(os.path.dirname(VV_VH_Cusum_file_low)) if (f.endswith('.shp')) \
                    & ('chunk' in f) & (str(threshold_low) in f) & (not str(threshold_high) in f)]
        high_list= [os.path.join(os.path.dirname(VV_VH_Cusum_file_low),f) for f in os.listdir(os.path.dirname(VV_VH_Cusum_file_low)) if (f.endswith('.shp')) \
                    & ('chunk' in f) & (str(threshold_high) in f) & (not str(threshold_low) in f)]
        low_list.sort()
        high_list.sort()
        pool = mp.Pool(nb_processeur_para)
        output_path = zip(*pool.starmap(parallel_cross_tc, zip(low_list,high_list)))   # PARALLEL COMPUTATION OVER AN INDEX OF LOW LIST 
        pool.close()
        output = None
        dirpath = os.path.dirname(low_list[0])
        
        cross_tc_shp_list = [os.path.join(dirpath,f) for f in os.listdir(dirpath) \
                         if ((f.endswith('.shp')) & ('chunk' in f) )]           #& (period in f)] if you have a period in your filename
        cross_tc_tif_list = list()
        print('---------------------------------\nSTARTING shp to tif conversion \n---------------------------------')    
        for shp_filepath in cross_tc_shp_list:
            print(os.path.basename(shp_filepath) +'\n')
            repair_shapefile(shp_filepath)
            output_filepath = shp_to_tif(shp_filepath, whole_zone_path)
            cross_tc_tif_list.append(output_filepath)
        print('---------------------------------\nSTARTING Reorganizing as single tif\n---------------------------------')    
        mosaic = gdal.Warp(cross_tc_tif_list[0].replace('chunk_1',''),cross_tc_tif_list,format='GTiff')        
        mosaic = None
        [remove_shapefile(f) for f in cross_tc_shp_list if os.path.exists(f)]       # Remove the shapefiles and subfiles after ending.
        choose_date_change_polarisation(cross_tc_tif_list[0].replace('chunk_1',''),VV_Cusum_file_low)