#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb 12 15:39:13 2024

@author: bygorra
"""

# NRT version of the CuSum

#------------------------------------------------------------------------------
#%% 0. Libraries to download
from matplotlib.lines import Line2D
from pyrasta.raster import Raster
from shapely.ops    import unary_union
from datetime       import datetime
from functools      import partial
from pathlib        import Path
from osgeo          import gdal,osr,ogr
from tqdm           import tqdm
from scipy          import ndimage
from gistools.layer import PolygonLayer
import matplotlib.pyplot as plt
import multiprocessing   as mp
import geopandas         as gpd
import numpy             as np
import fiona
import math
import os
import re
#------------------------------------------------------------------------------
#%% 1. Input parameters

    #%% 1.1. Thresholds, number of images before/after, area threshold, processing steps
    
cur_thresh_list     = np.array([92])                                            # Tc threshold for the series
threshold_high_list = np.array([100])                                           # Tc high threshold for post processing NOT USEFUL FOR FIELD MONITORING
threshold_low_list  = np.array([92])                                            # Tc low threshold for post processing NOT USEFUL FOR FIELD MONITORING

split_raster        = True                                                      # Split the CuSUm result raster in X chunks
slicing_number      = 4
nb_proc_para        = 4
# chunk_to_analyse    = [3,6,11,14,15]
chunk_to_analyse    = np.arange(1,17)
comp_bool           = True                                                      # Compute the CuSum
post_proc_bool      = False                                                     # Compute the post processing steps NOT USEFUL FOR FIELD MONITORING
histo_bool          = False                                                     # Compute the historic changes NOT USEFUL FOR FIELD MONITORING

nb_proc             = 4                                                         # Number of processors to allocate
nb_image_after_list  = np.array([3])                                            # Number of images to take after the wanted image
nb_image_before_list = np.array([11])                                           # Number of images to take before the wanted image
area_threshold_list  = np.arange(300,500,300)                                   # Area threshold for the Minimum Mapping Unit (square meters, recommended : 300)
hist_area_threshold  = 1000                                                     # Historic area threshold for MMU (square meters, recommended : 2000)                                                   
force_band = True
historic_period = '20180101_20200701'                                           # Period of the historic changes to detect
date_check_start = '20210101'                                                   # Date of the beginning of the monitoring period
date_check_stop  = '20220101'                                                   # Date of the end of the monitoring period
sous_period = date_check_start + '_' + date_check_stop


    #%% 1.2. Paths definition

# Need to define : the KML path to your area, the validation reference map (.tif), 
# The folderpath of the .tif bilateral7 images, the output folderpath for the change detection,
# The folderpath containing the files to compute stats on, the path to the output stats file


period = '20160101_20220101'                                                    # Period of monitoring

KML_path = os.path.abspath(os.path.join('path','to','kml.kml'))                 # Path to the KML file of your field



input_folderpath       = os.path.abspath(os.path.join('Folderpath','to folder','containing all images'))
output_folderpath = os.path.abspath(os.path.join('Folderpath','to folder','containing the output detection'))


output_shp_folderpath = os.path.abspath(os.path.join('Path','to put the shp files created'))

whole_zone_path   = os.path.abspath(os.path.join('Path','to a file being your area filled of 1s')) #image of 1 all over your area --> for counting
Historic_change_filepath = os.path.join('path','to your file containing FNF mask','.tif') #OPTIONAL

#------------------------------------------------------------------------------
#%% 2. Function parameters definition       
            
            
definitions = True
if definitions:
    
    #%% 2.1. General functions
    
    
    def open_tif(filepath,band_number):
        """
        Open the band_number band of the .tif file as array

        Parameters
        ----------
        filepath : str
            DESCRIPTION.
        band_number : int
            DESCRIPTION.

        Returns
        -------
        arr : numpy array
            DESCRIPTION.

        """
        img = gdal.Open(filepath)
        band = img.GetRasterBand(band_number)
        arr = band.ReadAsArray()
        arr[np.isnan(arr)]=0
        arr[np.isinf(arr)]=0
        arr[np.where(arr==999)]=0
        img = None
        return arr
    def find_dates(text):
        """
        Find the date in the filename.

        Parameters
        ----------
        text : str
            DESCRIPTION.

        Returns
        -------
        date : list of  datetime object
            DESCRIPTION.

        """
        match = re.search("(\d{8})", text)
        test = match[0]
        #print(test)
        date = list();
        if '20' in test[0:2]:
            if (int(test[2:4])<30) & (int(test[4:6])<=12) & (int(test[6:8])<=31):
                date.append(datetime.strptime(test, '%Y%m%d'))
        return date  
    def find_nearest_date(datelist,date):
        """
        Find the nearest date in the datelist of the 'date' input.

        Parameters
        ----------
        datelist : list of datetime objects
            DESCRIPTION.
        date : str (%Y%m%d') or datetime object
            DESCRIPTION.

        Returns
        -------
        date_to_check : TYPE
            DESCRIPTION.
        TYPE
            DESCRIPTION.

        """
        value_list = list()
        if type(date) == type('abc'):
            date_object = datetime.strptime(date,'%Y%m%d')
            date_output = date_object
        else:
            date_output = date
        for f in datelist:
            val= (f-date_output).days
            abs_val = abs(val)
            value_list.append(abs_val)
        index = np.where(np.array(value_list) == np.min(np.array(value_list)))

        date_to_check = np.array(datelist)[index]
        return date_to_check,index[0][0]
    def save_as_tif(raster_reference, input_array,nb_arrays, output_filepath):
        """
        Save arrays as bands in a .tif file

        Parameters
        ----------
        raster_reference : gdal.Open() object
            DESCRIPTION.
        input_array : numpy arrays
            Arrays to save in the .tif file
        nb_arrays : int
            DESCRIPTION.
        output_filepath : str
            DESCRIPTION.

        Returns
        -------
        None.

        """
        ds            = raster_reference;
        geotransform  = ds.GetGeoTransform();
        wkt           = ds.GetProjection();
        driver = gdal.GetDriverByName("GTiff");                                   # Create GTiff image
        
        if nb_arrays > 1:
            nb_row, nb_col = input_array[:,:,0].shape;
            dst_ds = driver.Create(output_filepath,nb_col,nb_row,nb_arrays,gdal.GDT_Float32)
            for curband in range(1,input_array.shape[2]+1):
                
                cur_array = input_array[:,:,curband-1]
                new_array = np.array(cur_array)

                #writting output raster
                dst_ds.GetRasterBand(curband).WriteArray( new_array )
                #setting nodata value
                dst_ds.GetRasterBand(curband).SetNoDataValue(0)
                #setting extension of output raster
                # top left x, w-e pixel resolution, rotation, top left y, rotation, n-s pixel resolution
                dst_ds.SetGeoTransform(geotransform)
                # setting spatial reference of output raster
                srs = osr.SpatialReference()
                srs.ImportFromWkt(wkt)
                dst_ds.SetProjection( wkt )
            #Close output raster dataset
        else:
            new_array = np.array(input_array)
            nb_row, nb_col = new_array.shape;
            dst_ds = driver.Create(output_filepath,nb_col,nb_row,nb_arrays,gdal.GDT_Float32)
            #writting output raster
            dst_ds.GetRasterBand(1).WriteArray( new_array )
            #setting nodata value
            dst_ds.GetRasterBand(1).SetNoDataValue(0)
            #setting extension of output raster
            # top left x, w-e pixel resolution, rotation, top left y, rotation, n-s pixel resolution
            dst_ds.SetGeoTransform(geotransform)
            # setting spatial reference of output raster
            srs = osr.SpatialReference()
            srs.ImportFromWkt(wkt)
            dst_ds.SetProjection( wkt )
    def progress_cb(complete, message, cb_data):
        '''Emit progress report in numbers for 10% intervals and dots for 3%'''
        if int(complete*100) % 10 == 0:
            print(f'{complete*100:.0f}', end='', flush=True)
        elif int(complete*100) % 3 == 0:
            print(f'{cb_data}', end='', flush=True)
    def gdal_mask(kml_path, input_filepath,output_filepath):
        """
        Remove pixels from input_filepath that are located outside of kml_path polygons
        Parameters
        ----------
        kml_path : str
            path to your vector file.
        input_filepath : str
            path to your .tif image.
    
        Returns
        -------
        str
            path to the output image.
    
        """
        options = {'cropToCutline':False,'dstNodata':0,\
        'cutlineDSName':kml_path,'callback': progress_cb,\
        'callback_data': '.'}
        gdal.Warp(output_filepath, input_filepath,options =gdal.WarpOptions(**options));    
    def extract_vv_vh(input_filepath,input_VV_folderpath,input_VH_folderpath):
        vh_raster = Raster(input_filepath).extract_bands([2])
        vv_raster = Raster(input_filepath).extract_bands([1])    
        vh_raster.to_file(os.path.join(input_VH_folderpath, os.path.basename(input_filepath)))
        vv_raster.to_file(os.path.join(input_VV_folderpath, os.path.basename(input_filepath)))         
    def extract_band(filepath_list):
        """
        This function is used to extract and store the VV and the VH data to \
            separate folders for RASTA to work.
        Parameters
        ----------
        input_folderpath : TYPE
            DESCRIPTION.
        sous_period_datetime : datetime object
            Monitored period to extract the image of.
    
        Returns
        -------
        None.
    
        """
        input_folderpath = os.path.dirname(filepath_list[0])
        output_VV_folder = os.path.join(input_folderpath,'VV')
        output_VH_folder = os.path.join(input_folderpath,'VH')
        Path(output_VV_folder).mkdir(parents=True,exist_ok=True)
        Path(output_VH_folder).mkdir(parents=True,exist_ok=True)
        
        filepath_list.sort()
        dirlist = filepath_list
        # print(len(dirlist))
        
        if len(dirlist)<20:
            dirlist1 = dirlist[0:len(dirlist)-1]
        elif (len(dirlist)>20) & (len(dirlist)<40):
            dirlist1 = dirlist[0:20]
            dirlist2 = dirlist[20:len(dirlist)-1]
        elif (len(dirlist)>40) & (len(dirlist)<60):
            dirlist1 = dirlist[0:20]
            dirlist2 = dirlist[20:40]
            dirlist3 = dirlist[40:len(dirlist)-1]
        elif (len(dirlist)>60) & (len(dirlist)<80):
            dirlist1 = dirlist[0:20]
            dirlist2 = dirlist[20:40]
            dirlist3 = dirlist[40:60]
            dirlist4 = dirlist[60:len(dirlist)-1]
        elif (len(dirlist)>80) & (len(dirlist)<100):
            dirlist1 = dirlist[0:20]
            dirlist2 = dirlist[20:40]
            dirlist3 = dirlist[40:60]
            dirlist4 = dirlist[60:80]
            dirlist5 = dirlist[80:len(dirlist)-1]
        if (len(dirlist)>100):
            dirlist1 = dirlist[0:20]
            dirlist2 = dirlist[20:40]
            dirlist3 = dirlist[40:60]
            dirlist4 = dirlist[60:80]
            dirlist5 = dirlist[80:100]
            dirlist6 = dirlist[100:len(dirlist)-1]            
            
        for filename in dirlist1:
            
                # print('Working on ',filename)
                extract_vv_vh(os.path.join(input_folderpath, filename),output_VV_folder,output_VH_folder)
        # print('\n--------------------------------------------\nExtraction of first 20 finished\n--------------------------------------------\n')
        if (len(dirlist)>20):
            for filename in dirlist2:
                # print('Working on ',filename)
                extract_vv_vh(os.path.join(input_folderpath, filename),output_VV_folder,output_VH_folder)
        # print('\n--------------------------------------------\nExtraction of first 40 finished\n--------------------------------------------\n')
        if (len(dirlist)>40):
            for filename in dirlist3:
                # print('Working on ',filename)
                extract_vv_vh(os.path.join(input_folderpath, filename),output_VV_folder,output_VH_folder)
        # print('\n--------------------------------------------\nExtraction of first 60 finished\n--------------------------------------------\n')
        if (len(dirlist)>60):
            for filename in dirlist4:
                # print('Working on ',filename)
                extract_vv_vh(os.path.join(input_folderpath, filename),output_VV_folder,output_VH_folder)
        # print('\n--------------------------------------------\nExtraction of first 80 finished\n--------------------------------------------\n')
        if (len(dirlist)>80):
            for filename in dirlist5:
                # print('Working on ',filename)
                extract_vv_vh(os.path.join(input_folderpath, filename),output_VV_folder,output_VH_folder)
        # print('\n--------------------------------------------\nExtraction of first 100 finished\n--------------------------------------------\n')
        if (len(dirlist)>100):
            for filename in dirlist6:
                # print('Working on ',filename)
                extract_vv_vh(os.path.join(input_folderpath, filename),output_VV_folder,output_VH_folder)     
        print('\n---------------------------------------------------------\nExtraction finished\n---------------------------------------------------------')
    def VV_VH(VV_filepath,VH_filepath):
        """
        Compute VV x VH combination

        Parameters
        ----------
        VV_filepath : str
            Path to the VV .tif file.
        VH_filepath : str
            Path to the VH .tif file.

        Returns
        -------
        None.

        """
        img_ref = gdal.Open(VV_filepath)
        VV = open_tif(VV_filepath,1)
        VH = open_tif(VH_filepath,1)
        
        VV[VV>0] = 1
        VH[VH>0] = 1
        
        VV_VH = VV * VH
        
        save_as_tif(img_ref,VV_VH,1,VV_filepath.replace('VV','cross_pol'))
        img_ref = None
    def compress(array):
        """ Compress 3D array (i.e. trim leading/trailing and inner zeros) along first axis
    
        Parameters
        ----------
        array
    
        Returns
        -------
    
        """
        if array.ndim != 3:
            return np.expand_dims(array, axis=0)
    
        output = np.flip(np.sort(array, axis=0), axis=0)
        output = output[output.any(axis=(1, 2)), :, :]
    
        return output
    def _multi_change(single_change, arrays, dates,
                      threshold, nb_samples, count=0):
    
        result = []
    
        change_before = np.zeros(single_change.shape)
        change_after = np.zeros(single_change.shape)
    
        for date in np.unique(single_change):
            if date != 0:
                idx_before = dates < date
                idx_after = dates > date
                # dates_before = dates[idx_before]
                # dates_after = dates[idx_after]
                # series_before = arrays[idx_before, :, :]
                # series_after = arrays[idx_after, :, :]
    
                for idx, change in zip([idx_before, idx_after],
                                       [change_before, change_after]):
                    _dates = dates[idx]
                    series = arrays[idx, :, :]
    
                    try:
                        chg = single_change_detection(series[:, single_change == date],
                                                      _dates, threshold, nb_samples)
                    except IndexError:
                        chg = np.zeros(1)
    
                    if np.all(chg == date):
                        pass
                    else:
                        if np.any(chg):
                            change[single_change == date] = chg
                            change_temp = np.zeros(single_change.shape)
                            change_temp[single_change == date] = chg
                            result.extend(_multi_change(change_temp, series,
                                                        _dates, threshold, nb_samples, count + 1))
    
                # try:
                #     chg_before = single_change_detection(series_before[:, single_change == date],
                #                                          dates_before, threshold, nb_samples)
                # except IndexError:
                #     chg_before = np.zeros(1)
                #
                # try:
                #     chg_after = single_change_detection(series_after[:, single_change == date],
                #                                         dates_after, threshold, nb_samples)
                # except IndexError:
                #     chg_after = np.zeros(1)
                #
                # if np.all(chg_before == date):
                #     pass
                # else:
                #     change_before[single_change == date] = chg_before
                #     change_before_temp = np.zeros(single_change.shape)
                #     change_before_temp[single_change == date] = chg_before
                #     if np.any(chg_before):
                #         result.extend(_multi_change(change_before_temp, series_before,
                #                                     dates_before, threshold, nb_samples, count + 1))
                #
                # if np.all(chg_after == date):
                #     pass
                # else:
                #     change_after[single_change == date] = chg_after
                #     change_after_temp = np.zeros(single_change.shape)
                #     change_after_temp[single_change == date] = chg_after
                #     if np.any(chg_after):
                #         result.extend(_multi_change(change_after_temp, series_after,
                #                                     dates_after, threshold, nb_samples, count + 1))
    
        if np.any(change_before):
            result.append(change_before)
    
        if np.any(change_after):
            result.append(change_after)
    
        return result
    def change_intensity(arrays, dates, threshold=.95, nb_samples=500):
        pass
    def tendency_change(arrays, dates, threshold=.95, nb_samples=500):
        """ Compute change in tendency
    
        Parameters
        ----------
        arrays
        dates
        threshold
        nb_samples
    
        Returns
        -------
    
        """
        change = single_change_detection(arrays, dates, threshold, nb_samples)
    
        changes = np.tile(np.ma.masked_equal(change, 0), (len(dates), 1, 1))
        mask_before = np.asarray([chg <= date for chg, date in zip(changes, dates)])
        mask_after = np.asarray([chg >= date for chg, date in zip(changes, dates)])
        changes_after = np.ma.masked_where(mask_before, arrays)
        changes_before = np.ma.masked_where(mask_after, arrays)
    
        tendency = np.asarray(np.mean(changes_after, axis=0) - np.mean(changes_before, axis=0))
        tendency[~np.isfinite(tendency)] = 0
    
        return tendency
    def multi_change_detection(arrays, dates, threshold=0.95, nb_samples=500):
        """ Detect multi change in raster window
    
        Parameters
        ----------
        arrays: list or numpy.ndarray
        dates: list or numpy.ndarray
        threshold: float
        nb_samples: int
    
        Returns
        -------
    
        """
        multi_change = np.zeros((len(dates),
                                 arrays[0].shape[0],
                                 arrays[0].shape[1]))
        multi_change[0, :, :] = single_change_detection(arrays, dates,
                                                        threshold, nb_samples)
        output = _multi_change(multi_change[0, :, :],
                               np.asarray(arrays),
                               dates,
                               threshold,
                               nb_samples)
    
        if output:
            output = compress(np.asarray(output))
            multi_change[1:len(output) + 1, :, :] = output
    
        return multi_change
    def single_change_detection(arrays, dates, threshold=0.95, nb_samples=500):
        """ Detect single change in raster window
    
        Parameters
        ----------
        arrays: list or numpy.ndarray
        dates: list or numpy.ndarray
        threshold: float
        nb_samples: int
    
        Returns
        -------
    
        """
        arrays = np.asarray(arrays)
        ci = np.ones(arrays[0].shape)
        change = np.zeros(arrays[0].shape)
        avg = np.mean(arrays, axis=0)
        cumsum = np.cumsum(arrays - avg, axis=0)
        rg = np.max(cumsum, axis=0) - np.min(cumsum, axis=0)
    
        rng = np.random.default_rng()
        bootstrap = rng.permutation(arrays)
    
        for _ in range(nb_samples):
            cumsum_bs = np.cumsum(bootstrap - avg, axis=0)
            rg_bs = np.max(cumsum_bs, axis=0) - np.min(cumsum_bs, axis=0)
    
            ci[rg_bs >= rg] -= 1/nb_samples  # >= and not strictly greater than (>)
    
            rng.shuffle(bootstrap)
    
        change[ci >= threshold] = 1
    
        return change * dates[np.argmax(cumsum, axis=0)]
    def main(in_dir, output_file, threshold, max_samples=500,
             nb_processes=int(mp.cpu_count()/2), window_size=100,
             chunksize=1, data_type="uint32", multi = False):
        
        images = []
        dates = []
    
        for name in os.listdir(in_dir):
            if  '.tif' in name:
                images.append(os.path.join(in_dir, name))
                dates.append(int(re.search(r'_(\d{8})T', name)[1]))
    
        images.sort()
        dates.sort()
        list_of_raster = [Raster(img) for img in images]
        dates = np.asarray(dates)
        nb_samples = min(max_samples, math.factorial(len(list_of_raster)))
        if multi:
            m = Raster.raster_calculation(list_of_raster, partial(multi_change_detection,
                                                                  dates=dates,
                                                                  threshold=threshold,
                                                                  nb_samples=nb_samples),
                                          input_type=gdal.GetDataTypeByName(data_type),
                                          output_type = gdal.GetDataTypeByName(data_type),
                                          window_size=window_size,
                                          nb_processes=nb_processes,
                                          chunksize=chunksize,
                                          no_data=0)
        else:
            m = Raster.raster_calculation(list_of_raster, partial(single_change_detection,
                                                                  dates=dates,
                                                                  threshold=threshold,
                                                                  nb_samples=nb_samples),
                                          input_type=gdal.GetDataTypeByName(data_type),
                                          output_type = gdal.GetDataTypeByName(data_type),
                                          window_size=window_size,
                                          nb_processes=nb_processes,
                                          chunksize=chunksize,
                                          no_data=0)
        m.to_file(output_file)
        m = None
    def remove_anterior_change(origin_file,historic_file,output_filepath):
        """
        Remove the changes found in historic file from the origin file.

        Parameters
        ----------
        origin_file : str
            Path to the file of the current monitoring period.
        historic_file : str
            Path to the change detection output file of the historic period.
        output_filepath : str
            DESCRIPTION.

        Returns
        -------
        None.

        """
        cur = open_tif(origin_file,1)
        img_ref = gdal.Open(origin_file)
        histo = open_tif(historic_file,1)
        ind = np.where(histo>0)
        
        cur[ind] = 0
        save_as_tif(img_ref,cur,1,output_filepath)
        img_ref = None
    def cusum_sing(filepath_list,polar,period, thresh):
        """
        Compute the CuSum single detection on the filepath_list for the Tc = thresh threshold.
        Can also compute the historic changes.

        Parameters
        ----------
        filepath_list : list of str
            List of the paths to the bilateral7 .tif files.
        histo_bool : bool
            Compute the changes on the historic period or not.
        period : str
            name the output bilan file.
        thresh : int
            Tc threshold.

        Returns
        -------
        TYPE
            DESCRIPTION.

        """
        print(filepath_list[0])
        input_VV_folderpath = os.path.join(os.path.dirname(filepath_list[0]),polar)
        output_VV = os.path.join(os.path.dirname(filepath_list[0]),'Change_detection','NRT','NRT_'+polar +'_' + period +'_' + str(thresh) + '.tif')
        # input_VH_folderpath = os.path.join(os.path.dirname(filepath_list[0]),'VH')
        # print(input_VV_folderpath + '\n\n\n')
        print('\n--------------------------------------------\nStarting CuSum on period:\n'+period+'\n--------------------------------------------\n')
        if os.path.exists(input_VV_folderpath):
            if len(os.listdir(input_VV_folderpath))>0:
                [os.remove(os.path.join(input_VV_folderpath,f))for f in os.listdir(input_VV_folderpath) if f.endswith('.tif')]
                # [os.remove(os.path.join(input_VH_folderpath,f))for f in os.listdir(input_VH_folderpath) if f.endswith('.tif')]
                # print('\n\nDirectory successfully removed ! ', os.listdir(input_VV_folderpath))
            # output_VH = os.path.join(os.path.dirname(filepath_list[0]),'Change_detection','NRT','NRT_VH_' + period +'_' + str(thresh) + '.tif')
        Path(os.path.dirname(output_VV)).mkdir(parents=True,exist_ok=True)
        
        extract_band(filepath_list)
        if os.path.exists(output_VV):
            print('CuSum not launching - file already existing')
        else:
            main(input_VV_folderpath, output_VV,\
                 threshold=thresh/100, max_samples=500,\
                 window_size=(20, 20), chunksize=1, nb_processes=nb_proc,multi=False)

        return output_VV
    def prepare_3d_arr(filepath,output_filepath,nb_3d):
        """
        Create a 3d numpy array that will contain all bands from the NRT bilan on the period.

        Parameters
        ----------
        filepath : TYPE
            DESCRIPTION.
        output_filepath : TYPE
            DESCRIPTION.
        nb_3d : TYPE
            DESCRIPTION.

        Returns
        -------
        output_array : TYPE
            DESCRIPTION.

        """
        print('\n\nPreparing array, number of 3d: ',nb_3d)
        arr    = open_tif(filepath   ,1)
        img_ref = gdal.Open(filepath)
        X,Y = arr.shape
        nb_ch = np.zeros(arr.shape)
        index = np.where(arr>0)
        nb_ch[index]=1
        output_array = np.zeros((X,Y,nb_3d+2))
        output_array[:,:,0] = arr
        output_array[:,:,1] = nb_ch
        output_array[:,:,2] = arr
        
        save_as_tif(img_ref,output_array,output_array.shape[2],output_filepath)
        img_ref = None
        return output_array
    def create_bilan(filelist,nb_before,nb_after,output_folderpath,thresh,nb_run,ref_period,cur_period):
        """
        Creates the bilan file by computing the cusum the first time and preparing
        the 3d arrays.

        Parameters
        ----------
        filelist : list of str
            List of the paths to the bilateral7 files.
        nb_before : int
            Number of images to take before the wanted date.
        nb_after : int
            Number of images to take after the wanted date.
        output_folderpath : str
            DESCRIPTION.
        thresh : int
            Tc threshold.
        nb_run : int
            Total number of iterations made by the NRT cusum during the bilan period.
        period : str
            DESCRIPTION.

        Returns
        -------
        TYPE
            DESCRIPTION.

        """
        print('\n---------------------------------------------------------\n CREATING BILAN for : nb img before : ' + str(nb_before) + '\nnb image after : ' + str(nb_image_after) + '\t threshold : ' + str(thresh) +'\n---------------------------------------------------------\n') 

        output_bilan_VV = os.path.join(output_folderpath,'VV_'    + ref_period +'_' + str(nb_before) + '_' + str(nb_after) + '_' + str(thresh) + 'bilan.tif')
        print(output_bilan_VV+'\n')
        if os.path.exists(output_bilan_VV):
            print('VV Already existing...')
            VV_bool = True
        else:
            output_VV =  cusum_sing(filelist,'VV',cur_period,thresh)
            prepare_3d_arr(output_VV   ,output_bilan_VV                      ,nb_run)
            VV_bool = False
        if os.path.exists(output_bilan_VV.replace('VV','VH'   )):
            print('VH Already existing...')
            VH_bool=True
            
        else:
            output_VH =  cusum_sing(filelist,'VH',cur_period,thresh)
            prepare_3d_arr(output_VH   ,output_bilan_VV.replace('VV','VH'   ),nb_run)
            VH_bool = False
        if os.path.exists(output_bilan_VV.replace('VV','cross')):
            cross_bool = True
        else:
            if not VV_bool:
                if not VH_bool:
                    pass
                else:
                    output_VH =  cusum_sing(filelist,'VH',cur_period,thresh)
            else:
                if not VH_bool: 
                    pass
                else:
                    output_VH =  cusum_sing(filelist,'VH',cur_period,thresh)
                output_VV =  cusum_sing(filelist,'VV',cur_period,thresh)
            output_cross_arr = VV_VH(output_VV, output_VH)
            output_cross =  output_VV.replace('VV','cross_pol')
    
            prepare_3d_arr(output_cross,output_bilan_VV.replace('VV','cross'),nb_run)
            cross_bool = False
        if not VV_bool:
            os.remove(output_VV   )
        if not VH_bool:
            os.remove(output_VH   )
        if not cross_bool:
            os.remove(output_cross)
        return output_bilan_VV,output_bilan_VV.replace('VV','VH'),output_bilan_VV.replace('VV','cross'),VV_bool,VH_bool,cross_bool
    def update_bilan(bilan_filepath,new_cusum_date_filepath,which_band):
        """
        Updates the bilan created in create_bilan with the cusum run on the newest 
        date.

        Parameters
        ----------
        bilan_filepath : str
            DESCRIPTION.
        new_cusum_date_filepath : str
            DESCRIPTION.
        which_band : int
            DESCRIPTION.

        Returns
        -------
        None.

        """
        img_ref = gdal.Open(new_cusum_date_filepath)
        bilan_raster = gdal.Open(bilan_filepath)
        nb_bands = bilan_raster.RasterCount        
        # print('\n\nFilename : ', os.path.basename(bilan_filepath))
        # print('\nUpdating with ', os.path.basename(new_cusum_date_filepath))
        cur_bilan   = open_tif(bilan_filepath         ,1)
        nb_ch_bilan = open_tif(bilan_filepath         ,2)
    
        cusum_arr   = open_tif(new_cusum_date_filepath,1)
        # print('Values : ', np.unique(cusum_arr))

        X,Y = cur_bilan.shape
        temp = np.zeros(cur_bilan.shape)
        # print('Cur bilan shape : ', cur_bilan.shape)
        # print('Cusum arr shape : ', cusum_arr.shape)
        
        index_cusum = np.where(cusum_arr>0)
        
        temp[index_cusum] = 1
        new_nb_ch_bilan = nb_ch_bilan + temp
        
        bad_index = np.where(cur_bilan == 1)
        temp2 = cusum_arr.copy()
        temp2[bad_index] = 0
        new_index = np.where(temp2>0)
        
                
        cur_bilan[new_index] = temp2[new_index]
        
        output_array = np.zeros((X,Y,nb_bands))
        output_array[:,:,0] = cur_bilan
        output_array[:,:,1] = new_nb_ch_bilan
        
        for nb_band in range(2,nb_bands-1):
            cur_band = bilan_raster.GetRasterBand(nb_band+1)
            cur_arr = cur_band.ReadAsArray()
            output_array[:,:,nb_band]=cur_arr
        output_array[:,:,which_band] = cusum_arr
        # print(np.unique(output_array[:,:,which_band]))
        save_as_tif(img_ref,output_array,nb_bands,bilan_filepath)
        img_ref = None
        os.remove(new_cusum_date_filepath)
    def dilate_erode(filepath,band):
        """
        Dilation and erosion of the matrix composing the bands of a multidetection .tif output
        to fill small gaps in the data.
    
        Parameters
        ----------
        filepath : str
            Path to the bilan .tif file.
        band : int
            Number of the band to dilate.
    
        Returns
        -------
        None.
    
        """
        # print('\n---------------------------------------------------------\nDilating, eroding ' + os.path.basename(filepath))
        # print('\n DIRECTORY PATH : ', os.path.dirname(filepath))
        img_file = gdal.Open(filepath)
        img_band = img_file.GetRasterBand(band)
        img_arr  = img_band.ReadAsArray()
        img_copy = img_arr.copy()
        output_ero = os.path.join(os.path.dirname(filepath),'dil_'+os.path.basename(filepath))
        img_arr[img_arr>0] = 1
        img_dil  = ndimage.binary_dilation(img_arr ).astype(img_arr.dtype )
        img_dil2 = ndimage.binary_dilation(img_dil ).astype(img_dil.dtype )
        img_ero1 = ndimage.binary_erosion (img_dil2).astype(img_dil2.dtype)
        img      = ndimage.binary_erosion (img_ero1).astype(img_ero1.dtype)
        index = np.where(img>0)
        output = img_copy.copy()
        index_ini = np.where(img_copy>0)
        output[index] = int(period[0:4]+'0000')
        output[index_ini] = img_copy[index_ini]
        save_as_tif(img_file,output,1,output_ero)
        img_file = None
        return output_ero    
    def binarize_image(filepath,band):
        """
        

        Parameters
        ----------
        filepath : str
            DESCRIPTION.
        band : int
            Band number to open. default 1.

        Returns
        -------
        output_filepath : str
            DESCRIPTION.

        """
        
        # print('\n---------------------------------------------------------\nBinarization of ' + os.path.basename(filepath))
        img_ref = gdal.Open(filepath)
        arr = open_tif(filepath,band)
        arr[arr>0]=1
        output_filepath = os.path.join(os.path.dirname(filepath),'bin'+os.path.basename(filepath))
        save_as_tif(img_ref,arr,1,output_filepath)
        img_ref = None
        os.remove(filepath)
        return output_filepath
    def polygonize_raster(input_filepath,output_filepath):
        """
        Polygonize the binary .tif file to .shp, using default 4-connectedness.

        Parameters
        ----------
        input_filepath : str
            DESCRIPTION.
        output_filepath : str
            DESCRIPTION.

        Returns
        -------
        None.

        """
        print('\n---------------------------------------------------------\nPolygonization of ' + os.path.basename(input_filepath))
        src_ds = gdal.Open(input_filepath)
        # print(output_filepath)
        srs = osr.SpatialReference()
        srs.ImportFromWkt(src_ds.GetProjection())
        srcband = src_ds.GetRasterBand(1)
        #
        #  create output datasource
        #
        dst_layername = "POLYGONIZED_STUFF"
        drv = ogr.GetDriverByName("ESRI Shapefile")
        dst_ds = drv.CreateDataSource( output_filepath)
        dst_layer = dst_ds.CreateLayer(dst_layername, srs = srs )
        DN_field = ogr.FieldDefn('DN', ogr.OFTReal)
        dst_layer.CreateField(DN_field)
        gdal.Polygonize( srcband, srcband, dst_layer, 0, [], callback=None )
        dst_ds = None
        src_ds = None
    def area_filter(geodataframe,area_threshold):
        """
        Code to filter a geodataframe according to the polygon area in square meters.
    
        Parameters
        ----------
        geodataframe : geopandas.geodataframe
            Geopandas geodataframe to remove of the polygons showing a lower area
            than the threshold.
        area_threshold : int
            Area threshold in square meters.
    
        Returns
        -------
        geodataframe
            geopandas geodataframe composed by polygons showing a higher area than the threshold.
        """
        temp = geodataframe.to_crs('EPSG:3857')
        mask = temp.area > area_threshold
        return geodataframe.loc[mask]
    def repair_shapefile(input_filepath):
        """
        Code to solve the self-intersection polygons of the polygonize_raster output
    
        Parameters
        ----------
        input_filepath : str
            Path to the .shp to repair the self intersections of.
    
        Returns
        -------
        None.
    
        """        
        
        # print('REPAIRING ', os.path.basename(input_filepath))
        gk500 = ogr.Open(input_filepath, 1) # <====== 1 = update mode
        gk_lyr = gk500.GetLayer()
        # print(len(gk_lyr), ' features to check')
        for feature in gk_lyr:
            # logi += 1
            # if len(gk_lyr)!=0:
                # if logi%int(len(gk_lyr)/10) == 0:
                    # print( str(logi/len(gk_lyr)*100).split('.')[0], ' % PROGRESS')
            geom = feature.GetGeometryRef()
            if not geom.IsValid():
                feature.SetGeometry(geom.Buffer(0)) # <====== SetGeometry
                gk_lyr.SetFeature(feature) # <====== SetFeature
                assert feature.GetGeometryRef().IsValid() # Doesn't fail
    
        gk_lyr.ResetReading()
        assert all(feature.GetGeometryRef().IsValid() for feature in gk_lyr)
        # print('\nREPARATION FINISHED')
    def polygonize_image(bin_filepath):
        # print('\n---------------------------------------------------------\nSTARTING POLYGONIZATION\n')
        # print('WORKING ON ', os.path.basename(bin_filepath))
        shape_folderpath = os.path.join(os.path.dirname(bin_filepath),'polygons')
        Path(shape_folderpath).mkdir(parents=True,exist_ok=True)

        shape_filepath = os.path.join(shape_folderpath,os.path.basename(bin_filepath).replace('tif','shp').replace('bin','shape'))
        polygonize_raster(bin_filepath,shape_filepath)

        # os.remove(bin_filepath)
        repair_shapefile(shape_filepath)
        return shape_filepath
    def remove_shapefile(shapefile):
        os.remove(shapefile)
        os.remove(shapefile.replace('.shp','.shx') )
        if os.path.exists(shapefile.replace('.shp','.prj')):
            os.remove(shapefile.replace('.shp','.prj') )
        os.remove(shapefile.replace('.shp','.dbf') )
        if os.path.exists(shapefile.replace('.shp','.cpg')):
            os.remove(shapefile.replace('.shp','.cpg') )
    def method_5(high_tc,low_tc,area_threshold):
        """
        Cross-Tc computation : Working on the 'intersection' rule. If a low Tc polygon shows
        AT LEAST one intersection with a high tc polygon, it is not removed.
    
        Parameters
        ----------
        high_tc : geopandas file
            High Tc (100) geopandas opened file.
        low_tc : geopandas file
            Low Tc (75) geopandas opened file.
        area_threshold : int
            Threshold based on the area (square meters) to remove the polygons showing
            a lower area.
    
        Returns
        -------
        output : geopandas geodataframe
            DESCRIPTION.
    
        """        
        
        crs_tc = high_tc.to_crs('EPSG:3857')
        crs_low = low_tc.to_crs('EPSG:3857')
        filtered_high_tc = area_filter(crs_tc,area_threshold )
        filtered_low_tc  = area_filter(crs_low,area_threshold)
        with fiona.Env(OSR_WKT_FORMAT="WKT2_2018"):
            filtered_high_crs = filtered_high_tc.to_crs('EPSG:4326')
            filtered_low_crs  = filtered_low_tc .to_crs('EPSG:4326')
        if not filtered_high_crs.empty:
            with fiona.Env(OSR_WKT_FORMAT="WKT2_2018"):


                # print('area filtered shapefile : ',filtered_high_crs,'\n length of low tc : ',len(filtered_low_crs.geometry))
                # geohigh = PolygonLayer.from_collection(os.path.abspath(os.path.join(os.getcwd(),'temp_high.shp')))
                output_high = os.path.abspath(os.path.join(os.getcwd(),'temp_high.shp'))
                duplicate_count = 1
                exist = False
                if os.path.exists(output_high):
                    a = output_high.replace('.shp','_' + str(duplicate_count) + '.shp')
                    if os.path.exists(a):
                        exist = True
                    else:
                        b = a
                    while exist == True:
                        duplicate_count += 1

                        b = os.path.abspath(os.path.join(os.getcwd(),'temp_high' + str(duplicate_count) + '.shp'))
                        if os.path.exists(b):
                            exist = True
                        else:
                            exist = False
                        print('While loop still working : ', duplicate_count, ' ',b)
                else:
                    b = output_high
                    
                output_low = b.replace('high','low')
                filtered_high_crs.to_file   (driver = 'ESRI Shapefile', filename = b)
                filtered_low_crs .to_file   (driver = 'ESRI Shapefile', filename = output_low)            
            geohigh = PolygonLayer(b)
            geolow = PolygonLayer(output_low)
            bool_output = geolow.intersects(geohigh)
            output = geolow[bool_output]
            # print('TEST ON GEOLAYER METHOD : ', output)
            remove_shapefile(b)
            remove_shapefile(output_low)
            temp =4
        else:
            output = False
            temp = 0
        return output,temp        
    def cross_tc(high_tc_file,low_tc_file,thresh,thresh_high,area_threshold):
        """
        Code to apply the spatial cross-Tc recombination to the high and low Tc 
        shapefiles. Only polygons larger than the area threshold (square meters)
        in both shapefiles are available for the recombination.

        Parameters
        ----------
        high_tc_file : str
            Path to the high Tc (100) file.
        low_tc_file : str
            Path to the low Tc (75) file.
        thresh : int
            Low Tc threshold (75).
        thresh_high : int
            High Tc threshold (100).
        area_threshold : int
            Threshold (square meters) removing polygons < threshold from the recombination.

        Returns
        -------
        output_filepath : str
            DESCRIPTION.

        """
        print('\n---------------------------------------------------------\nSTARTING CROSS-TC COMPUTATION OF ',os.path.basename(low_tc_file),'\n')
        high_tc_pd = gpd.read_file(high_tc_file)
        low_tc_pd  = gpd.read_file(low_tc_file )
        output_cross,value_indicator = method_5(high_tc_pd,low_tc_pd,area_threshold)  
        output_filepath = low_tc_file.replace('_'+str(thresh),'_'+str(thresh_high) + '_' + str(thresh)).replace('.shp','_'+str(area_threshold)+'.shp')
        # print(output_cross)
        # print(output_filepath)
        if value_indicator>2:
            if type(output_cross) == type(str('STRING')):
                # print('\n\n\n\nCROSS-TC COMPUTATION FINISHED\n\nNO CHANGES FOUND\n\n')
                os.remove(high_tc_file)
                os.remove(low_tc_file)
            else:
                output_cross.to_file   (driver = 'ESRI Shapefile', file_path = output_filepath)
                # print('\nCROSS-TC COMPUTATION FINISHED')
                repair_shapefile(output_filepath)
                os.remove(high_tc_file)
                os.remove(low_tc_file)
                remove_shapefile(high_tc_file)
                remove_shapefile(low_tc_file)
           
                
        return output_filepath,value_indicator
    def cross_tc_shp_to_tif(cross_tc_shp_filepath,low_tc_tif_filepath,threshold_low,threshold_high,area_threshold):
        """
        Conversion of the vector file .shp to raster file .tif

        Parameters
        ----------
        cross_tc_shp_filepath : str
            Path to the cross tc vector file.
        low_tc_tif_filepath : str
            Path to the lower tc in the cross tc, raster .tif file.
        area_threshold : int
            For naming files.

        Returns
        -------
        output_filepath : str
            DESCRIPTION.

        """
        output_folder = os.path.join(os.path.dirname(low_tc_tif_filepath),'cross_TC_tif')
        Path(output_folder).mkdir(parents=True,exist_ok=True)
        output_filepath = os.path.join(output_folder,os.path.basename(low_tc_tif_filepath).replace('_'+str(threshold_low),'_'+str(threshold_high) + '_' + str(threshold_low)).replace('.tif','_'+str(area_threshold)+'.tif'))
        if os.path.exists(cross_tc_shp_filepath):
            # print('\n CONVERTING ' + os.path.basename(cross_tc_shp_filepath) + ' TO TIF FILE : ' + os.path.basename(output_filepath))
            
            gdal_mask(cross_tc_shp_filepath,low_tc_tif_filepath,output_filepath)
            
            # os.remove(cross_tc_shp_filepath)
            # os.remove(cross_tc_shp_filepath.replace('.shp','.shx'))
            # if os.path.exists(cross_tc_shp_filepath.replace('.shp','.prj')):
            #     os.remove(cross_tc_shp_filepath.replace('.shp','.prj'))
            # os.remove(cross_tc_shp_filepath.replace('.shp','.dbf'))
            # if os.path.exists(cross_tc_shp_filepath.replace('.shp','.cpg')):
            #     os.remove(cross_tc_shp_filepath.replace('.shp','.cpg'))                
            
            # print('\nCONVERTION DONE')
        else:
            pass
            # print('No polygon available for this tc / period combinations')
        return output_filepath
    def shp_to_tif(input_filepath_shp,tif_filepath):
        """
        Conversion of the vector file .shp to raster file .tif

        Parameters
        ----------
        cross_tc_shp_filepath : str
            Path to the cross tc vector file.
        low_tc_tif_filepath : str
            Path to the lower tc in the cross tc, raster .tif file.
        area_threshold : int
            For naming files.

        Returns
        -------
        output_filepath : str
            DESCRIPTION.

        """
        output_folder = os.path.join(os.path.dirname(input_filepath_shp),'chunk_tif')
        Path(output_folder).mkdir(parents=True,exist_ok=True)
        output_filepath = os.path.join(output_folder,os.path.basename(input_filepath_shp).replace('.shp','.tif'))
        if os.path.exists(input_filepath_shp):
            # print('\n CONVERTING ' + os.path.basename(cross_tc_shp_filepath) + ' TO TIF FILE : ' + os.path.basename(output_filepath))
            
            gdal_mask(input_filepath_shp,tif_filepath,output_filepath)
            
            # os.remove(cross_tc_shp_filepath)
            # os.remove(cross_tc_shp_filepath.replace('.shp','.shx'))
            # if os.path.exists(cross_tc_shp_filepath.replace('.shp','.prj')):
            #     os.remove(cross_tc_shp_filepath.replace('.shp','.prj'))
            # os.remove(cross_tc_shp_filepath.replace('.shp','.dbf'))
            # if os.path.exists(cross_tc_shp_filepath.replace('.shp','.cpg')):
            #     os.remove(cross_tc_shp_filepath.replace('.shp','.cpg'))                
            
            # print('\nCONVERTION DONE')
        else:
            pass
            # print('No polygon available for this tc / period combinations')
        return output_filepath
    def split_translate(input_filepath,chunk_number,x,y,x1,y1):
 
        output_filepath = os.path.join(os.path.dirname(input_filepath),os.path.basename(input_filepath).replace('.tif','_chunk_'+str(chunk_number)+'.tif'))
        # print('...................SPLITTING DATA.................')
        com_string = "gdal_translate -srcwin " + str(x)+ " " + str(y)+ " " + str(x1) + " " + str(y1) + " "  + input_filepath + " " + output_filepath 
        os.system(com_string)
        return output_filepath    
    
    def histo_parallel_cross_tc(chunk_filepath_low,chunk_filepath_high):
        print('polygonization has not bugged')
        # print(input_filepath_index)
        pol_filepath_low  = polygonize_image(chunk_filepath_low )
        pol_filepath_high = polygonize_image(chunk_filepath_high)                    
        
        # pol_filepath_low = output_low_list [input_filepath_index-1]
        output_filepath_shp,value_indicator = \
        cross_tc(pol_filepath_high,pol_filepath_low,threshold_low,threshold_high,hist_area_threshold)                 
        print('cross tc has not bugged')
        return output_filepath_shp
    
    def post_processing_cascade (filepath_low,filepath_high,band,threshold_low,threshold_high,area_threshold):
        """
        CuSum post processing cascade according to YGORRA B., et al, 2022.
            It is composed by several steps : dilatation/erosion, binarization
            for polygonization,
            Area filtering (Minimum Mapping Unit)
            cross-Tc recombination
            Conversion from VECTOR file to RASTER file. OPTIONAL
        Parameters
        ----------
        filepath_low : str
            Path of the low Tc (75) bilan .tif file.
        filepath_high : str
            Path of the high Tc (100) bilan .tif file.
        band : int
            Number of the band to work on.
        threshold_low : int
            Tc low (75).
        threshold_high : int
            Tc high (100).
        area_threshold : int
            Threshold (square meters) removing polygons < threshold from the recombination.

        Returns
        -------
        output_filepath : str
            DESCRIPTION.

        """
        # global histo_parallel_cross_tc
        dilated_low_filepath  = dilate_erode(filepath_low ,band)
        dilated_high_filepath = dilate_erode(filepath_high,band)
        
        bin_filepath_low  = binarize_image(dilated_low_filepath ,1 )
        bin_filepath_high = binarize_image(dilated_high_filepath,1)
        
        # os.remove(dilated_low_filepath)
        # os.remove(dilated_high_filepath)
        

        if split_raster:
            output_filepath_list = list()
            output_low_list      = list()
            output_high_list     = list()
            
         
            img = open_tif(bin_filepath_low,1)
            nb_Y,nb_X = img.shape
            tilesize_X = int(nb_X/slicing_number)
            tilesize_Y = int(nb_Y/slicing_number)
            chunk_number = 1
            
            # print('\n\nBIN FILEPATH : ' +bin_filepath_low+'\n\n\n')
            for slice_x in range(slicing_number):
                x  = (slice_x  )*int(nb_X/slicing_number)
                
                if slice_x == slicing_number -1:
                    tilesize_X = int(nb_X-x)
                for slice_y in range(slicing_number):
                    y  = (slice_y  )*int(nb_Y/slicing_number)
                    if slice_y == slicing_number -1:
                        tilesize_Y = int(nb_Y-y)
                    # print('\nx value :\t',x,'\ttile size X value :\t', tilesize_X,'\ny value :\t',y,'\tTile size Y value :\t',tilesize_Y,'\n\n')
                    chunk_filepath_low = split_translate(bin_filepath_low , chunk_number, x, y, tilesize_X, tilesize_Y)
                    chunk_filepath_high= split_translate(bin_filepath_high, chunk_number, x, y, tilesize_X, tilesize_Y)
                    output_low_list .append(chunk_filepath_low )
                    output_high_list.append(chunk_filepath_high)
                    chunk_number+=1
            # print(output_high_list)
            
                
 
            if __name__ == '__main__':
                pool = mp.Pool(nb_proc_para)
                # output_path = zip(*pool.map(parallel_cross_tc, range(len(output_high_list)))) 
                
                
                print('Starting parallel cross-Tc computation')
                output_path = zip(*pool.starmap(histo_parallel_cross_tc, zip(output_low_list,output_high_list)))  
                
                
                # print('\n\nOUTPUT PATH :',output_path)
                # output_folder = os.path.dirname(output_path)
                # print(output_shp_folderpath)
                cross_tc_list = [os.path.join(output_shp_folderpath,f) for f in os.listdir(output_shp_folderpath) if ((f.endswith('.shp')) & ('chunk' in f) & (period in f))]
                cross_tc_tif_list = list()
                # print('\nCROSSTCLIST\n' ,cross_tc_list)
                
                for shp_filepath in cross_tc_list:
                    # print('\nWorking on ' + os.path.basename(shp_filepath))
                    output_filepath = shp_to_tif(shp_filepath, whole_zone_path)
                    cross_tc_tif_list.append(output_filepath)
                cross_tc_tif_list.sort()
                # print('\n',cross_tc_tif_list)
                
                # mosaic_filepath = os.path.join(stats_input_folderpath_cross_tc,os.path.basename(cross_tc_tif_list[0]))
                output_filepath = os.path.join(stats_input_folderpath_cross_tc,os.path.basename(dilated_low_filepath).replace(str(threshold_low),str(threshold_high) + '_' + str(threshold_low)).replace('dil_',''))
                # print('\n'+mosaic_filepath)
                mosaic = gdal.Warp(output_filepath,cross_tc_tif_list,format='GTiff')        
                mosaic = None
                [remove_shapefile(f) for f in cross_tc_list if os.path.exists(f)]
        
        else:
        
            pol_filepath_low  = polygonize_image(bin_filepath_low )
            pol_filepath_high = polygonize_image(bin_filepath_high)
            
            output_filepath_shp, value_indicator = cross_tc(pol_filepath_high,pol_filepath_low,threshold_low,threshold_high,hist_area_threshold)
            output_filepath = cross_tc_shp_to_tif(output_filepath_shp,os.path.join(output_folderpath,'cross_pol_' + period + '_' + str(threshold_high) +'_' + str(threshold_low) +'.tif'),threshold_low,threshold_high,hist_area_threshold)    
        return output_filepath
    def NRT_simulate(date_check_start,date_check_end,nb_image_before,nb_image_after,\
                     threshold,filelist,output_folderpath):
        """
        Simulation of the Near Real Time (NRT) CuSum between the Date_check_start
        and the Date_check_end period. 
        
        It works as an iteration on the images during
        the period, taking nb_image_before before the iteration date and nb_image_after
        after the iteration date. 
        
        Tc, the critical threshold (see Ygorra et al, 2021 for precisions) is computed as
        threshold.
        
        The filelist is composed by the preprocessed Sentinel-1 dual pol images for the complete
        period composed by (1) historic period and (2) monitoring period.
        
        Parameters
        ----------
        date_check_start : str or datetime object
            Format '%Y%m%d' if a string. Date of the start of the monitoring period.
        date_check_end : str or datetime object
            Format '%Y%m%d' if a string. Date of the end  of the monitoring period.
        nb_image_before : int
            Number of images before the iteration date to consider in the cusum computation.
        nb_image_after : int
            Number of images before the iteration date to consider in the cusum computation.
        threshold : int
            Tc (critical threshold) to compute the CuSum on.
        filelist : list of str
            List of the paths to the preprocessed Sentinel-1 VV/VH .tif images.
        histo_bool : bool
            True to compute the historic, False not to.
        output_folderpath : str
            Path to the output folder.

        Returns
        -------
        TYPE
            DESCRIPTION.

        """
        filelist.sort()
        datelist =[find_dates(os.path.basename(f))[0] for f in filelist]
        start_date, index_start = find_nearest_date(datelist, date_check_start)
        end_date  , index_end   = find_nearest_date(datelist, date_check_end  )
        
        filelist_arr = np.array(filelist)
        # period = start_date[0].strftime('%Y%m%d') + '_' + end_date[0].strftime('%Y%m%d')    
        input_filelist = filelist_arr[index_start-nb_image_before:index_start+nb_image_after+1]
        nb_run = int(index_end-index_start)
        
        
        
        ref_period = date_check_start + '_'+ date_check_end
        cur_period = datelist[index_start-nb_image_before].strftime('%Y%m%d') \
                +'_'+ datelist[index_start + nb_image_after+1].strftime('%Y%m%d')
        print('\nCheckupdate : ref period',ref_period,'\n\n vs cur period:',cur_period)
        
        
        output_VV_bilan  , output_VH_bilan , output_cross_bilan,VV_bool,VH_bool,cross_bool   = create_bilan(\
                    input_filelist,nb_image_before,nb_image_after,output_folderpath,threshold ,nb_run,ref_period,cur_period)
        
        # if (not VV_bool) or (not VH_bool):
        for index_date,which_band in zip(range(index_start,index_end+1),range(2,nb_run+2)):
            period = datelist[index_date- nb_image_before].strftime('%Y%m%d') \
                +'_'+ datelist[index_date + nb_image_after+1].strftime('%Y%m%d')
            input_filelist  = filelist_arr[index_date-nb_image_before:index_date+nb_image_after+1]
            # print('\n---------------------------------------------------------\n           COMPUTING CUSUM\n')
            output_VV = cusum_sing(input_filelist, 'VV', period, threshold)
            output_VH = cusum_sing(input_filelist, 'VH', period, threshold)
            VV_VH(output_VV,output_VH)
            
            
            
            
            output_cross = output_VV.replace('VV','cross_pol')
            # print('\n---------------------------------------------------------')
            # print('\n           UPDATING BILAN on band '+str(which_band+2)+'\n')
            # print('\n\n Period :  ',find_dates(os.path.basename(input_filelist[0]))[0].strftime('%Y%m%d'),'    to    ',\
                  # find_dates(os.path.basename(input_filelist[len(input_filelist)-1]))[0].strftime('%Y%m%d'))
            
            update_bilan(output_VV_bilan   ,output_VV   ,which_band)
            update_bilan(output_VH_bilan   ,output_VH   ,which_band)
            update_bilan(output_cross_bilan,output_cross,which_band)
        if os.path.exists(output_VV):
            os.remove(output_VV)
            os.remove(output_VH)
            os.remove(output_VV.replace('VV','cross_pol'))
        return output_VV_bilan,output_VH_bilan,output_cross_bilan
    def NRT_post_proc(bilan_low_path=None,bilan_high_path=None,output_hist_cross_tc_mask = Historic_change_filepath,threshold_low=80,threshold_high=100,force_band = True, area_threshold=300):
        """
        CuSum post processing cascade according to YGORRA B., et al, 2022.
        
            It is composed by several steps : 
            
            Removal of anterior changes
                
            dilatation/erosion, binarization
            for polygonization,
            
            Area filtering (Minimum Mapping Unit)
            cross-Tc recombination
            
            Conversion from VECTOR file to RASTER file. OPTIONAL
        Parameters
        ----------
        filepath_low : str
            Path of the low Tc (75) bilan .tif file.
        filepath_high : str
            Path of the high Tc (100) bilan .tif file.
        band : int
            Number of the band to work on.
        threshold_low : int
            Tc low (75).
        threshold_high : int
            Tc high (100).
        area_threshold : int
            Threshold (square meters) removing polygons < threshold from the recombination.

        Returns
        -------
        output_filepath : str
            Path to the output bilan .tif file.

        """        

        print('\n---------------------------------------------------------\nAPPLYING POST PROCESSING TO\n' + \
               os.path.basename(bilan_low_path))
         
        mask_arr = open_tif(output_hist_cross_tc_mask,1)
        anterior_changes_index = np.where (mask_arr>0)
        
        img_ref = gdal.Open(bilan_low_path)
        temp = open_tif(bilan_low_path,1)
        X,Y = temp.shape
        if force_band:
            nb_bands = 1
            arr_3d = np.zeros((X,Y,1))
            arr_low  = open_tif(bilan_low_path ,1)
            arr_high = open_tif(bilan_high_path,1)
            arr_low [anterior_changes_index] = 0
            arr_high[anterior_changes_index] = 0            
            output_path_low =  os.path.join(os.path.dirname(bilan_low_path ),'templow_'  + os.path.basename(bilan_low_path ))
            output_path_high = os.path.join(os.path.dirname(bilan_high_path),'temphigh_' + os.path.basename(bilan_high_path))
            save_as_tif(img_ref,arr_low ,1,output_path_low )
            save_as_tif(img_ref,arr_high,1,output_path_high)
            output_filepath = post_processing_cascade (output_path_low,output_path_high,1,\
                            threshold_low,threshold_high,area_threshold)
            if output_filepath:
                if os.path.exists(output_filepath):
                    arr_output = open_tif(output_filepath,1)
                    arr_3d[:,:,0] = arr_output
                    os.remove(output_filepath )
            
        else:
            nb_bands = img_ref.RasterCount
            arr_3d = np.zeros((X,Y,nb_bands))
            for band in range(1,nb_bands+1):
                
                
                
                arr_low  = open_tif(bilan_low_path ,band)
                arr_high = open_tif(bilan_high_path,band)
                arr_low [anterior_changes_index] = 0
                arr_high[anterior_changes_index] = 0            
                output_path_low =  os.path.join(os.path.dirname(bilan_low_path ),'templow_'  + os.path.basename(bilan_low_path ))
                output_path_high = os.path.join(os.path.dirname(bilan_high_path),'temphigh_' + os.path.basename(bilan_high_path))
                save_as_tif(img_ref,arr_low ,1,output_path_low )
                save_as_tif(img_ref,arr_high,1,output_path_high)
                output_filepath = post_processing_cascade (output_path_low,output_path_high,1,\
                                threshold_low,threshold_high,area_threshold)
                if output_filepath:
                    if os.path.exists(output_filepath):
                        arr_output = open_tif(output_filepath,1)
                        arr_3d[:,:,band-1] = arr_output
                        os.remove(output_filepath )
    
                os.remove(output_path_low )
                os.remove(output_path_high)                          
                
                

        output_filepath = os.path.join(os.path.dirname(bilan_low_path),'cross_TC_tif',os.path.basename(bilan_low_path).replace('_'+str(threshold_low),'_'+str(threshold_high)+'_'+str(threshold_low)).replace('.tif','_'+str(area_threshold)+'.tif'))
        if force_band:
            arr_output = arr_3d[:,:,0]
            save_as_tif(img_ref,arr_output,1,output_filepath)
        else:
            save_as_tif(img_ref,arr_3d,nb_bands,output_filepath)
        img_ref = None
        return output_filepath


#-------------------------
#%% 3. Application of CuSum 
#       3.1. Creation of the filelist / datelist associated
#-------------------------
filepath_list = [os.path.join(input_folderpath,f.replace('S1B','S1A')) for f in os.listdir(input_folderpath) if (f.endswith('.tif'))]
filepath_list.sort()
datelist = [find_dates(os.path.basename(f))[0] for f in filepath_list]

start_date, index_start = find_nearest_date(datelist, sous_period.split('_')[0])
end_date  , index_end   = find_nearest_date(datelist, sous_period.split('_')[1])
#       3.2. Creation of the historic changes ( = FNF mask) OPTIONAL
#-------------------------
if histo_bool:
    
    end_date, index_end = find_nearest_date(datelist, historic_period.split('_')[1])
    # print(index_end)
    hist_path_list = filepath_list[0:index_end]
    extract_band(hist_path_list)
    main(os.path.join(os.path.dirname(hist_path_list[0]),'VV'), os.path.join(output_folderpath,'VV_' + historic_period +'_80.tif'),\
          threshold=0.8, max_samples=500,\
          window_size=(20, 20), chunksize=1, nb_processes=nb_proc,multi=False)    
    main(os.path.join(os.path.dirname(hist_path_list[0]),'VH'), os.path.join(output_folderpath,'VH_' + historic_period +'_80.tif'),\
          threshold=0.8, max_samples=500,\
          window_size=(20, 20), chunksize=1, nb_processes=nb_proc,multi=False)        
    main(os.path.join(os.path.dirname(hist_path_list[0]),'VV'), os.path.join(output_folderpath,'VV_' + historic_period +'_100.tif'),\
          threshold=1, max_samples=500,\
          window_size=(20, 20), chunksize=1, nb_processes=nb_proc,multi=False)    
    main(os.path.join(os.path.dirname(hist_path_list[0]),'VH'), os.path.join(output_folderpath,'VH_' + historic_period +'_100.tif'),\
          threshold=1, max_samples=500,\
          window_size=(20, 20), chunksize=1, nb_processes=nb_proc,multi=False)          
    VV_VH(os.path.join(output_folderpath,'VV_' + historic_period +'_100.tif'), os.path.join(output_folderpath,'VH_' + historic_period +'_100.tif'))
    VV_VH(os.path.join(output_folderpath,'VV_' + historic_period +'_80.tif' ), os.path.join(output_folderpath,'VH_' + historic_period +'_80.tif'))
    
    dilated_high_filepath = dilate_erode(os.path.join(output_folderpath,'cross_pol_' + historic_period +'_100.tif') ,1)
    dilated_low_filepath  = dilate_erode(os.path.join(output_folderpath,'cross_pol_' + historic_period +'_80.tif' ) ,1)
    
    # print('DILATED FILEPATHS :\n',dilated_high_filepath,'\n',dilated_low_filepath)
    bin_filepath_low = binarize_image(dilated_low_filepath,1)
    bin_filepath_high = binarize_image(dilated_high_filepath,1)
    
    # os.remove(dilated_low_filepath)
    # os.remove(dilated_high_filepath)
    
    
    if split_raster:
        output_filepath_list = list()
        output_low_list      = list()
        output_high_list     = list()
        
     
        img = open_tif(bin_filepath_low,1)
        nb_Y,nb_X = img.shape
        tilesize_X = int(nb_X/slicing_number)
        tilesize_Y = int(nb_Y/slicing_number)
        chunk_number = 1
        
        print('\n\nBIN FILEPATH : ' +bin_filepath_low+'\n\n\n')
        for slice_x in range(slicing_number):
            x  = (slice_x  )*int(nb_X/slicing_number)
            
            if slice_x == slicing_number -1:
                tilesize_X = int(nb_X-x)
            for slice_y in range(slicing_number):
                y  = (slice_y  )*int(nb_Y/slicing_number)
                if slice_y == slicing_number -1:
                    tilesize_Y = int(nb_Y-y)
                # print('\nx value :\t',x,'\ttile size X value :\t', tilesize_X,'\ny value :\t',y,'\tTile size Y value :\t',tilesize_Y,'\n\n')
                chunk_filepath_low = split_translate(bin_filepath_low , chunk_number, x, y, tilesize_X, tilesize_Y)
                chunk_filepath_high= split_translate(bin_filepath_high, chunk_number, x, y, tilesize_X, tilesize_Y)
                output_low_list .append(chunk_filepath_low )
                output_high_list.append(chunk_filepath_high)
                chunk_number+=1
        print(output_high_list)
        
            
        def histo_parallel_cross_tc(input_filepath_index):
            chunk_filepath_low  = output_low_list [input_filepath_index-1]
            chunk_filepath_high = output_high_list[input_filepath_index-1]
            
            # print(input_filepath_index)
            pol_filepath_low  = polygonize_image(chunk_filepath_low )
            pol_filepath_high = polygonize_image(chunk_filepath_high)                    
            
            # pol_filepath_low = output_low_list [input_filepath_index-1]
            output_filepath_shp,value_indicator = \
            cross_tc(pol_filepath_high,pol_filepath_low,80,100,hist_area_threshold)                 
            return output_filepath_shp           
        
        if __name__ == '__main__' :
            pool                = mp.Pool(nb_proc_para)
            # output_path = zip(*pool.map(parallel_cross_tc, range(len(output_high_list)))) 
            
            output_path = zip(*pool.map(histo_parallel_cross_tc, chunk_to_analyse))    
        print('\n\nOUTPUT PATH :',output_path)
        output_folder = os.path.dirname(output_path)
        print(output_shp_folderpath)
        cross_tc_list = [os.path.join(output_shp_folderpath,f) for f in os.listdir(output_shp_folderpath) if (f.endswith('.shp') & ('chunk' in f))]
        cross_tc_tif_list = list()
        # print('\nCROSSTCLIST\n' ,cross_tc_list)
        
        for shp_filepath in cross_tc_list:
            print('\nWorking on ' + os.path.basename(shp_filepath))
            output_filepath = shp_to_tif(shp_filepath, whole_zone_path)
            cross_tc_tif_list.append(output_filepath)
        cross_tc_tif_list.sort()
        # print('\n',cross_tc_tif_list)
        
        # mosaic_filepath = os.path.join(stats_input_folderpath_cross_tc,os.path.basename(cross_tc_tif_list[0]))
        mosaic_filepath = Historic_change_filepath
        print('\n'+mosaic_filepath)
        mosaic = gdal.Warp(mosaic_filepath,cross_tc_tif_list,format='GTiff')        
        mosaic = None
        [remove_shapefile(f) for f in cross_tc_list if os.path.exists(f)]
    
    else:
        pol_filepath_low  = polygonize_image(bin_filepath_low )
        pol_filepath_high = polygonize_image(bin_filepath_high)
        
        output_filepath_shp, value_indicator = cross_tc(pol_filepath_high,pol_filepath_low,80,100,hist_area_threshold)
        cross_tc_shp_to_tif(output_filepath_shp,os.path.join(output_folderpath,'cross_pol_' + historic_period +'_80.tif'),80,100,hist_area_threshold)    
    

if comp_bool:
    # comp_filelist = [os.path.join(input_folderpath,f) for f in os.listdir(os.path.join(input_folderpath,'VV')) if (f.endswith('.tif'))]
    # comp_filelist.sort()
    for nb_image_after in nb_image_after_list:
        for nb_image_before in nb_image_before_list:
            for cur_thresh in cur_thresh_list:
                
                print('\n---------------------------------------------------------\nStarting computation\n---------------------------------------------------------\n')
                
                
                
                Path(output_folderpath).mkdir(parents=True,exist_ok=True)
                
                # datelist =[find_dates(os.path.basename(f))[0] for f in filepath_list]
                # date_start_str = datetime.strptime(date_check_start,'%Y%m%d')
                
                # start_date, index_start = find_nearest_date(datelist, date_start_str)
                # end_date, index_stop = find_nearest_date(datelist, datetime.strptime(date_check_stop,'%Y%m%d'))
                
                output_VV_bilan_low,output_VH_bilan_low,output_cross_bilan_low = \
                    NRT_simulate(date_check_start,date_check_stop,nb_image_before,nb_image_after,cur_thresh,\
                                 filepath_list,output_folderpath) 
                # print(output_VV_bilan_low)
# remove_list = [os.path.join(output_folderpath,f) for f in os.listdir(output_folderpath) if 'temp' in f]
# for f in remove_list:
#     os.remove(f)                       
                    
if post_proc_bool:
    for nb_image_after in nb_image_after_list:
        for nb_image_before in nb_image_before_list:
            for area_threshold in area_threshold_list:
                for threshold_high in threshold_high_list:
                    for threshold_low in threshold_low_list:
                        output_VV_bilan_low     = os.path.join(output_folderpath,'VV_'    + sous_period + '_' + str(nb_image_before) + '_' + str(nb_image_after) + '_' + str(threshold_low) + 'bilan.tif')
                        output_VH_bilan_low     = os.path.join(output_folderpath,'VH_'    + sous_period + '_' + str(nb_image_before) + '_' + str(nb_image_after) + '_' + str(threshold_low) + 'bilan.tif')
                        output_cross_bilan_low  = os.path.join(output_folderpath,'cross_' + sous_period + '_' + str(nb_image_before) + '_' + str(nb_image_after) + '_' + str(threshold_low) + 'bilan.tif')
                        
                        output_VV_bilan_high    = os.path.join(output_folderpath,'VV_'    + sous_period + '_' + str(nb_image_before) + '_' + str(nb_image_after) + '_' + str(threshold_high) + 'bilan.tif')
                        output_VH_bilan_high    = os.path.join(output_folderpath,'VH_'    + sous_period + '_' + str(nb_image_before) + '_' + str(nb_image_after) + '_' + str(threshold_high) + 'bilan.tif')
                        output_cross_bilan_high = os.path.join(output_folderpath,'cross_' + sous_period + '_' + str(nb_image_before) + '_' + str(nb_image_after) + '_' + str(threshold_high) + 'bilan.tif')                      

                        NRT_post_proc(output_VV_bilan_low   ,output_VV_bilan_high   ,Historic_change_filepath,threshold_low,threshold_high,force_band,area_threshold)
                        NRT_post_proc(output_VH_bilan_low   ,output_VH_bilan_high   ,Historic_change_filepath,threshold_low,threshold_high,force_band,area_threshold)
                        NRT_post_proc(output_cross_bilan_low,output_cross_bilan_high,Historic_change_filepath,threshold_low,threshold_high,force_band,area_threshold)



