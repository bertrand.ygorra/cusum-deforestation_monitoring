#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Function repertory for the CuSum
Created on Thu Mar 24 09:49:38 2022

@author: bygorra

"""
import math
import os
import re
from functools           import partial
from pyrasta.raster      import Raster
from pathlib             import Path
from osgeo               import gdal
from datetime            import datetime
import multiprocessing as mp
import numpy           as np
if 'Travail_z' in os.getcwd():
    os.chdir("..")
    os.chdir("..")


def find_dates(text):
    match = re.search("(\d{8})", text)
    test = match[0]
    print(test)
    date = list();
    if '20' in test[0:2]:
        if (int(test[4:6])<=12) & (int(test[6:8])<=31):
            date.append(datetime.strptime(test, '%Y%m%d'))
    return date[0]

def open_tif(filepath,band_number):
    """
    Open the band_number band of the .tif file as array

    Parameters
    ----------
    filepath : str
        DESCRIPTION.
    band_number : int
        DESCRIPTION.

    Returns
    -------
    arr : numpy array
        DESCRIPTION.

    """
    img = gdal.Open(filepath)
    band = img.GetRasterBand(band_number)
    arr = band.ReadAsArray()
    img = None
    return arr
def extract_vv_vh(input_folderpath,sous_period_datetime,filename):
    """
    Extract the VV and VH bands to separate folders. Structure required for main.

    Parameters
    ----------
    input_folderpath : TYPE
        DESCRIPTION.
    sous_period_datetime : TYPE
        DESCRIPTION.
    filename : TYPE
        DESCRIPTION.

    Returns
    -------
    None.

    """
    sous_period_str = datetime.strftime(sous_period_datetime[0],'%Y%m%d')+'_'+datetime.strftime(sous_period_datetime[1],'%Y%m%d')
    Path(os.path.join(input_folderpath,sous_period_str+'_VH')).mkdir(parents=True,exist_ok=True)
    Path(os.path.join(input_folderpath,sous_period_str+'_VV')).mkdir(parents=True,exist_ok=True)
    
    vh_raster = Raster(os.path.join(input_folderpath, filename)).extract_bands([2])
    vv_raster = Raster(os.path.join(input_folderpath, filename)).extract_bands([1])    
    
    vh_raster.to_file(os.path.join(os.path.join(input_folderpath,sous_period_str+'_VH'), filename))
    vv_raster.to_file(os.path.join(os.path.join(input_folderpath,sous_period_str+'_VV'), filename))    


def extract_band(input_folderpath,sous_period_datetime):
    """
    This function is used to extract and store the VV and the VH data to separate folders for PYRASTA to work.

    Parameters
    ----------
    input_folderpath : str
        Path to the flder containing your files.
    sous_period_datetime : datetime object
        Monitored period to extract the image of.

    Returns
    -------
    None.

    """
    
    dirlist_temp = [f for f in os.listdir(input_folderpath) if f.endswith('.tif')]
    dirlist_temp.sort()
    # print(dirlist_temp)
    datelist = [find_dates(f) for f in dirlist_temp]
    dirlist = list()
    for date, name in zip(datelist,dirlist_temp):
        if (date>= sous_period_datetime[0]) & (date<sous_period_datetime[1]): 
            dirlist.append(name)
            
            
    total_length = len(dirlist)
    list_of_list = [[] for i in range(int(len(dirlist)/20)+1)]
    counter = 0
    
    list_init = list()
    for file, count in zip(dirlist,range(len(dirlist))):
        if count%20!=0:
            list_init.append(file)
        elif (count%20==0) & (count !=0):
            list_of_list[counter] = list_init
            list_init = list()
            counter +=1
    
    print( '-------------------------------------------------\n Starting extraction ',len(dirlist),'\t remaining')
    
    # print(list_of_list)
    print('And its length : ', len(list_of_list))
    for filelist, number in zip(list_of_list, range(len(list_of_list))):
    
        for filename in filelist:
            extract_vv_vh(input_folderpath,sous_period_datetime, filename)
        print((number +1)*20, 'Achieved. Starting up to ',(number +2)*20)  
            
            
def compress(array):
    """ Compress 3D array (i.e. trim leading/trailing and inner zeros) along first axis

    Parameters
    ----------
    array

    Returns
    -------

    """
    if array.ndim != 3:
        return np.expand_dims(array, axis=0)

    output = np.flip(np.sort(array, axis=0), axis=0)
    output = output[output.any(axis=(1, 2)), :, :]

    return output


def _multi_change(single_change, arrays, dates,
                  threshold, nb_samples, count=0):

    result = []

    change_before = np.zeros(single_change.shape)
    change_after = np.zeros(single_change.shape)

    for date in np.unique(single_change):
        if date != 0:
            idx_before = dates < date
            idx_after = dates > date
            # dates_before = dates[idx_before]
            # dates_after = dates[idx_after]
            # series_before = arrays[idx_before, :, :]
            # series_after = arrays[idx_after, :, :]

            for idx, change in zip([idx_before, idx_after],
                                   [change_before, change_after]):
                _dates = dates[idx]
                series = arrays[idx, :, :]

                try:
                    chg = single_change_detection(series[:, single_change == date],
                                                  _dates, threshold, nb_samples)
                except IndexError:
                    chg = np.zeros(1)

                if np.all(chg == date):
                    pass
                else:
                    if np.any(chg):
                        change[single_change == date] = chg
                        change_temp = np.zeros(single_change.shape)
                        change_temp[single_change == date] = chg
                        result.extend(_multi_change(change_temp, series,
                                                    _dates, threshold, nb_samples, count + 1))

            # try:
            #     chg_before = single_change_detection(series_before[:, single_change == date],
            #                                          dates_before, threshold, nb_samples)
            # except IndexError:
            #     chg_before = np.zeros(1)
            #
            # try:
            #     chg_after = single_change_detection(series_after[:, single_change == date],
            #                                         dates_after, threshold, nb_samples)
            # except IndexError:
            #     chg_after = np.zeros(1)
            #
            # if np.all(chg_before == date):
            #     pass
            # else:
            #     change_before[single_change == date] = chg_before
            #     change_before_temp = np.zeros(single_change.shape)
            #     change_before_temp[single_change == date] = chg_before
            #     if np.any(chg_before):
            #         result.extend(_multi_change(change_before_temp, series_before,
            #                                     dates_before, threshold, nb_samples, count + 1))
            #
            # if np.all(chg_after == date):
            #     pass
            # else:
            #     change_after[single_change == date] = chg_after
            #     change_after_temp = np.zeros(single_change.shape)
            #     change_after_temp[single_change == date] = chg_after
            #     if np.any(chg_after):
            #         result.extend(_multi_change(change_after_temp, series_after,
            #                                     dates_after, threshold, nb_samples, count + 1))

    if np.any(change_before):
        result.append(change_before)

    if np.any(change_after):
        result.append(change_after)

    return result


def change_intensity(arrays, dates, threshold=.95, nb_samples=500):
    pass


def tendency_change(arrays, dates, threshold=.95, nb_samples=500):
    """ Compute change in tendency

    Parameters
    ----------
    arrays
    dates
    threshold
    nb_samples

    Returns
    -------

    """
    change = single_change_detection(arrays, dates, threshold, nb_samples)

    changes = np.tile(np.ma.masked_equal(change, 0), (len(dates), 1, 1))
    mask_before = np.asarray([chg <= date for chg, date in zip(changes, dates)])
    mask_after = np.asarray([chg >= date for chg, date in zip(changes, dates)])
    changes_after = np.ma.masked_where(mask_before, arrays)
    changes_before = np.ma.masked_where(mask_after, arrays)

    tendency = np.asarray(np.mean(changes_after, axis=0) - np.mean(changes_before, axis=0))
    tendency[~np.isfinite(tendency)] = 0

    return tendency


def multi_change_detection(arrays, dates, threshold=0.95, nb_samples=500):
    """ Detect multi change in raster window

    Parameters
    ----------
    arrays: list or numpy.ndarray
    dates: list or numpy.ndarray
    threshold: float
    nb_samples: int

    Returns
    -------

    """
    multi_change = np.zeros((len(dates),
                             arrays[0].shape[0],
                             arrays[0].shape[1]))
    multi_change[0, :, :] = single_change_detection(arrays, dates,
                                                    threshold, nb_samples)
    output = _multi_change(multi_change[0, :, :],
                           np.asarray(arrays),
                           dates,
                           threshold,
                           nb_samples)

    if output:
        output = compress(np.asarray(output))
        multi_change[1:len(output) + 1, :, :] = output

    return multi_change


def single_change_detection(arrays, dates, threshold=0.95, nb_samples=500):
    """ Detect single change in raster window

    Parameters
    ----------
    arrays: list or numpy.ndarray
    dates: list or numpy.ndarray
    threshold: float
    nb_samples: int

    Returns
    -------

    """
    arrays = np.asarray(arrays)
    ci = np.ones(arrays[0].shape)
    change = np.zeros(arrays[0].shape)
    avg = np.mean(arrays, axis=0)
    cumsum = np.cumsum(arrays - avg, axis=0)
    rg = np.max(cumsum, axis=0) - np.min(cumsum, axis=0)

    rng = np.random.default_rng()
    bootstrap = rng.permutation(arrays)

    for _ in range(nb_samples):
        cumsum_bs = np.cumsum(bootstrap - avg, axis=0)
        rg_bs = np.max(cumsum_bs, axis=0) - np.min(cumsum_bs, axis=0)

        ci[rg_bs >= rg] -= 1/nb_samples  # >= and not strictly greater than (>)

        rng.shuffle(bootstrap)

    change[ci >= threshold] = 1

    return change * dates[np.argmax(cumsum, axis=0)]


def main(in_dir, output_file, threshold, max_samples=500,
         nb_processes=int(mp.cpu_count()/2), window_size=100,
         chunksize=1, data_type="uint32", multi = False):
    
    images = []
    dates = []

    for name in os.listdir(in_dir):
        if  '.tif' in name:
            images.append(os.path.join(in_dir, name))
            dates.append(int(re.search(r'_(\d{8})T', name)[1]))

    images.sort()
    dates.sort()
    list_of_raster = [Raster(img) for img in images]
    dates = np.asarray(dates)
    nb_samples = min(max_samples, math.factorial(len(list_of_raster)))
    if multi:
        m = Raster.raster_calculation(list_of_raster, partial(multi_change_detection,
                                                              dates=dates,
                                                              threshold=threshold,
                                                              nb_samples=nb_samples),
                                      input_type=gdal.GetDataTypeByName(data_type),
                                      output_type = gdal.GetDataTypeByName(data_type),
                                      window_size=window_size,
                                      nb_processes=nb_processes,
                                      chunksize=chunksize,
                                      no_data=0)
    else:
        m = Raster.raster_calculation(list_of_raster, partial(single_change_detection,
                                                              dates=dates,
                                                              threshold=threshold,
                                                              nb_samples=nb_samples),
                                      input_type=gdal.GetDataTypeByName(data_type),
                                      output_type = gdal.GetDataTypeByName(data_type),
                                      window_size=window_size,
                                      nb_processes=nb_processes,
                                      chunksize=chunksize,
                                      no_data=0)
    m.to_file(output_file)


# if multi_change:
#     main(vv_dir, os.path.join(img_dir,'Change_detection',sous_period+'_VV_Multi_Change_'+str(thresh)+'.tif'),
#           threshold=thresh/100, max_samples=200,
#           window_size=(20, 20), chunksize=1, nb_processes=8,multi=multi_change)
#     main(vh_dir, os.path.join(img_dir,'Change_detection',sous_period+'_VH_Multi_Change_'+str(thresh)+'.tif'),
#           threshold=thresh/100, max_samples=200,
#           window_size=(20, 20), chunksize=1, nb_processes=8,multi=multi_change)
# else:    
#     main(vv_dir, os.path.join(img_dir,'Change_detection',sous_period+'_VV_Single_Change_'+str(thresh)+'.tif'),
#           threshold=thresh/100, max_samples=200,
#           window_size=(20, 20), chunksize=1, nb_processes=8,multi=multi_change)
#     main(vh_dir, os.path.join(img_dir,'Change_detection',sous_period+'_VH_Single_Change_'+str(thresh)+'.tif'),
#           threshold=thresh/100, max_samples=200,
#           window_size=(20, 20), chunksize=1, nb_processes=8,multi=multi_change)




