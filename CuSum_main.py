#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar 24 15:46:39 2022

@author: bygorra
"""

from CuSum_functions import extract_band,main
from osgeo           import gdal
from datetime        import datetime
from pathlib         import Path
import warnings
import os

warnings.filterwarnings("ignore")
gdal.SetConfigOption('CPL_TMPDIR', '/home/teledec/bygorra/tmp/')
multi_change       = True
extract_dates      = True
change_computation = True
threshold_tc = 75 #Critical threshold (Tc)
sous_period = '20180101_20190701' #Monitoring period you want to work on, start_date YYYYMMDD, end_date YYYYMMDD
sous_period_datetime = [datetime.strptime(str(sous_period.split('_')[0]),'%Y%m%d'),datetime.strptime(str(sous_period.split('_')[1]),'%Y%m%d')]

img_dir = os.path.abspath(os.path.join('Path to the directory containing your S1 images'))
vh_dir = os.path.join(img_dir,sous_period+"_VH")
vv_dir = os.path.join(img_dir,sous_period+"_VV")
Path(os.path.join(img_dir,'Change_detection')).mkdir(parents=True,exist_ok=True)

if extract_dates:
    extract_band(img_dir,sous_period_datetime)
    
if change_computation:
    main(vv_dir, os.path.join(img_dir,'Change_detection',sous_period+'_VV_Multi_Change_'+str(threshold_tc)+'.tif'),
         threshold=threshold_tc/100, max_samples=200,
         window_size=(20, 20), chunksize=1, nb_processes=8,multi=multi_change)
    main(vh_dir, os.path.join(img_dir,'Change_detection',sous_period+'_VH_Multi_Change_'+str(threshold_tc)+'.tif'),
         threshold=threshold_tc/100, max_samples=200,
         window_size=(20, 20), chunksize=1, nb_processes=8,multi=multi_change)



