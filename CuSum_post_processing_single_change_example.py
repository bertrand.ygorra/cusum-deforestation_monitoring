#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar 24 16:11:01 2022

@author: bygorra
"""

import os 
import numpy as np
from tqdm                  import tqdm
from osgeo                 import gdal
from datetime              import datetime
from CuSum_preprocessing   import save_as_tif,open_tif
from CuSum_post_processing import band_sum,remove_nb_change

from CuSum_post_processing import dilate_erode,VV_VH,zone_img_creation,remove_anterior_change,binarization,polygonize_raster,repair_shapefile, cross_tc,shp_to_tif
#%%------------------------------------------------------------------------------
#    STEP 1 : VV x VH combination, dilation /erosion, number of changes 
#        Number of changes threshold (Tnbc), historic removal
#--------------------------------------------------------------------------------
# Recommended steps :
# (1) Transformation to single change maps
# (2) Dilatation, erosion
# (3) VV x VH combination
# (4) Historic changes removal   
# (5) Tnbc application : number of changes removal
dil_ero               = True                                                                 # Dilatation and erosion, then erosion + dilatation to join roads
VV_VH_computation     = True                                                                 # Computation of VV intersect VH change rasters
remove_historic_pixel = True
ReCuSum               = True
remove_high_nb_change = False
nb_change_threshold_high = 8
nb_change_threshold_low  = 18
VV_Cusum_file_low  = os.path.abspath(os.path.join('your folder path','your VV filename low .tif'))   # Absolute path to your file containing the low tc cusum  VV result from cusum.py
VV_Cusum_file_high = os.path.abspath(os.path.join('your folder path','your VV filename high.tif'))   # Absolute path to your file containing the high tc cusum VV result from cusum.py

VH_Cusum_file_low  = os.path.abspath(os.path.join('your folder path','your VH filename low .tif'))   # Absolute path to your file containing the low tc cusum  VH result from cusum.py
VH_Cusum_file_high = os.path.abspath(os.path.join('your folder path','your VH filename high.tif'))   # Absolute path to your file containing the high tc cusum VH result from cusum.py

whole_zone_path = os.path.join(os.path.dirname(VV_Cusum_file_low),'whole_zone.tif') # whole_zone.tif is a file composed of 1s with the same size as the above files.
if not os.path.exists(whole_zone_path):                                     # If this file does not exist, we create it
    zone_img_creation(VV_Cusum_file_low,whole_zone_path)

if remove_historic_pixel:
    Historic_filepath = os.path.abspath(os.path.join('your folder path absolute', 'your historic filename.tif')) # Absolute path to your file containing the NF mask (forest coded as >0) / CuSum cross-Tc on anterior period


if remove_high_nb_change or ReCuSum:
    VH_nb_change_filepath_95  = os.path.abspath(os.path.join('your folder path','your map of number of changes of VH filename 95 .tif'))   # Absolute path to your file containing the low tc cusum  VH 95 result from ReCuSum post processing
    VH_nb_change_filepath_100 = os.path.abspath(os.path.join('your folder path','your map of number of changes of VH filename 100 .tif'))  # Absolute path to your file containing the low tc cusum  VH 100 result from ReCuSum post processing

if ReCuSum:
    VH_filepath_95  = os.path.abspath(os.path.join('your folder path','your VH filename 95 .tif'))   # Absolute path to your file containing the low tc cusum  VH 95 result from cusum.py as multi-detection 
    VH_filepath_100 = os.path.abspath(os.path.join('your folder path','your VH filename 100 .tif'))   # Absolute path to your file containing the low tc cusum  VH 100 result from cusum.py as multi-detection


if dil_ero:
# Closing : dilatation dilatation erosion erosion to close gaps / connects roads
    print('Dilating, eroding....................')
    dil_VV_file_low  = dilate_erode(VV_Cusum_file_low )                         
    dil_VV_file_high = dilate_erode(VV_Cusum_file_high)
    dil_VH_file_low  = dilate_erode(VH_Cusum_file_low )
    dil_VH_file_high = dilate_erode(VH_Cusum_file_high)
                
if VV_VH_computation:
# Intersection of VV result map with VH result map : Greatly decrease false positive, slightly increase the false negative.
    print('Intersecting VV with VH....................')
    VV_VH_low  = VV_VH(dil_VV_file_low ,dil_VH_file_low )
    VV_VH_high = VV_VH(dil_VV_file_high,dil_VH_file_high)
    
if remove_historic_pixel:
# Historic pixels are pixels determined as change before the monitoring period. = Non-Forest mask
    print('Removing anterior pixels....................')
    remove_anterior_change(VV_VH_low ,Historic_filepath)
    remove_anterior_change(VV_VH_high,Historic_filepath)
          
if ReCuSum: 
    band_sum(VH_filepath_95 ,VH_nb_change_filepath_95 )
    band_sum(VH_filepath_100,VH_nb_change_filepath_100)
    
if remove_high_nb_change:
    remove_nb_change(nb_change_threshold_low,nb_change_threshold_high,VV_VH_low ,VH_nb_change_filepath_95,VH_nb_change_filepath_100)
    remove_nb_change(nb_change_threshold_low,nb_change_threshold_high,VV_VH_high,VH_nb_change_filepath_95,VH_nb_change_filepath_100)
        
"""
#%%------------------------------------------------------------------------------ 
# STEP 2 : Spatial recombination of the temporal results : cross-Tc
#--------------------------------------------------------------------------------

# Recommended steps :
# (1) Binarization of both high and low tc maps (100 and 75), in all polarizations
# (2) Conversion of both high/low tc binar maps to shapefiles
# (3) Spatial recombination : cross-tc
# (4) Conversion from cross-tc shape to cross-tc in tif
"""
binar                   = True
polygonize              = True
repair                  = True
cross_tc_bool           = True
save_tif                = True
apply_to_nb_change      = False
historic_pixels_removal = True
nb_change_threshold     = False
area_threshold          = 300                                                   # Minimum mapping unit
threshold_low = 75                                                          # Lower value of the Critical Threshold (Tc). Usually 75
threshold_high = 100                                                        # Higher value of the Critical Threshold (Tc). Usually 100

VV_VH_Cusum_file_low  = os.path.abspath(os.path.join('your folder path','your VV_VH filename low tc.tif')) 
VV_VH_Cusum_file_high = os.path.abspath(os.path.join('your folder path','your VV_VH filename high tc.tif'))

if binar:
    VV_VH_bin_low  = VV_VH_Cusum_file_low .replace('.tif','bin.tif')        # Name of the binary .tif file created
    VV_VH_bin_high = VV_VH_Cusum_file_high.replace('.tif','bin.tif')
    
    binarization(VV_VH_Cusum_file_low ,VV_VH_bin_low )                      # Binarization of the low file
    binarization(VV_VH_Cusum_file_high,VV_VH_bin_high)

if polygonize:
    print('-------------------\nSTARTING POLYGONIZATION........\n-------------------')
    VV_VH_low_pol = VV_VH_low .replace('.tif','.shp')
    VV_VH_high_pol= VV_VH_high.replace('.tif','.shp')

    polygonize_raster(VV_VH_low ,VV_VH_low_pol )                    # Polygonize the binary raster. 0s are NaN
    polygonize_raster(VV_VH_high,VV_VH_high_pol)
    
    os.remove(VV_VH_bin_low )
    os.remove(VV_VH_bin_high)
    
    if repair:
        print('-------------------\nSTARTING SHP REPAIRS........\n-------------------')
        repair_shapefile(VV_VH_low_pol )
        repair_shapefile(VV_VH_high_pol)

if cross_tc:
    print('---------------------------------\nSTARTING CROSS-TC COMPUTATION........\n---------------------------------')
    print('\nCross-tc creation in shapefiles')
    output_filepath_shp,value_indicator = cross_tc(VV_VH_high_pol,VV_VH_low_pol,threshold_low,threshold_high,area_threshold)    
    if value_indicator >=2:
        output_filepath = shp_to_tif(output_filepath_shp, whole_zone_path)



