# CuSum-Deforestation_monitoring

CuSum - Multidetection and bilan for tropical deforestation monitoring

This repository contains multiple files, including modules such as CuSum_functions.py and CuSum_post_processing.py. These modules contain functions useful for working with tif georeferenced files. 
They also contain the function used as post-processing steps for the cross-threshold recombination, Forest/Non-forest mask removal and the application of the threshold based on the number of changes.
Files contained in work_examples are scripts with parameters to compute the post-processing steps / the main cusum function.

The processing is described in Ygorra et al, 2021 (https://doi.org/10.1016/j.jag.2021.102532)  and Ygorra et al, 2023 (https://doi.org/10.1016/j.isprsjprs.2023.08.006)

These scripts aim at detecting change in forest using time-series of Sentinel-1 images preprocessed (including speckle filter). See Manogaran et al 2018, Kellndorfer et al 2019 (SAR handbook by SERVIR - chapter 3), Ygorra et al, 2021a / 2021b / 2022 / 2023 for more informations.

If you use this repository in a scientific publication, please cite the correct reference article (CuSum cross-Tc : Ygorra et al, 2021b, ReCuSum : Ygorra et al 2022 / 2023)

## Acknowledgements
This work was done during my Ph.D funded by VisioTerra, developed at INRAE (UMR ISPA) and the cusum main function optimisation was done by Benjamin Pillot using https://github.com/benjamin-pillot/gis-tools.
The continuation of this work on new areas / for new objectives is being done at INRIA (GEOSTAT TEAM)



## Project status
In progress
